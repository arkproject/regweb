package it.arkproject.view.Util;

import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Named;

@Named
@Stateless
public class ReceiptBean {
	 Date date = new Date();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    

}
