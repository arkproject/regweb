package it.arkproject.view.Util;

import javax.inject.Named;

@Named
public class NavigationManager {
	private String nomeScheda;
	private String hRef;

	public String getNomeScheda() {
		return nomeScheda;
	}

	public String gethRef() {
		return hRef;
	}

	// Restituisce il nome della scheda partendo dalla classe
	// di appartenenza
	public void setNomeScheda(String simpleName) {
		for (EntitaNomeScheda n : EntitaNomeScheda.values()) {
			if (n.name().equals(simpleName)) {
				this.nomeScheda = n.getText();
				this.hRef = n.gethRef();
				break;
			}
		}
	}

	// Restituisce hRef della scheda partendo dal nome della scheda
	public static String getHRef(String nomeScheda) {
		for (EntitaNomeScheda n : EntitaNomeScheda.values()) {
			if (n.getText().equals(nomeScheda)) {
				nomeScheda = n.gethRef();
				break;
			}
		}
		return nomeScheda;
	}
	
	public static enum EntitaNomeScheda {
		Anagraficapaziente("Anagrafica Paziente","/pages/secure/paziente/anagrafica/edit"), 
		Demografica("Demografica","/pages/secure/paziente/anagrafica/schede/demografica"), 
		Indirizzoresidenza("Indirizzo di residenza","/pages/secure/paziente/anagrafica/schede/indirizzo"), 
		Mmse("M.M.S.E.","/pages/secure/paziente/test/mmse"), 
		Cirs("C.I.R.S","/pages/secure/paziente/test/cirs");

		private String text;
		private String hRef;

		private EntitaNomeScheda(String text) {
			this.text = text;
		}

		public String getText() {
			return text;
		}

		public String gethRef() {
			return hRef;
		}

		public void sethRef(String hRef) {
			this.hRef = hRef;
		}

		private EntitaNomeScheda(String nomeScheda, String hRef) {
			this.text = nomeScheda;
			this.hRef = hRef;
		}

	}
}
