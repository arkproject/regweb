package it.arkproject.view;

import java.util.Date;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.arkproject.genericClass.AbstractBean;
import it.arkproject.model.Indirizzoresidenza;


 
@Named
@ConversationScoped
public  class IndirizzoresidenzaBean extends AbstractBean<Indirizzoresidenza>  {
	
	
	private static final long serialVersionUID = 1L;

    public void onDateSelect(SelectEvent event) {
    	this.setDataCompilazione((Date) event.getObject());    
    }


}