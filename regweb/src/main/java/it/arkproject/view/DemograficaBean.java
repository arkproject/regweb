package it.arkproject.view;

import it.arkproject.genericClass.AbstractBean;
import it.arkproject.model.Demografica;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ConversationScoped
public class DemograficaBean extends AbstractBean<Demografica> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
  

	private List<SelectItem> itemYesNo;

	public List<SelectItem> getItemYesNo() {
		return itemYesNo;
	}

	public void setItemYesNo(List<SelectItem> itemYesNo) {
		this.itemYesNo = itemYesNo;
	}

	public List<SelectItem> fillItemYesNo() {
		itemYesNo = new ArrayList<>();
		itemYesNo.add(new SelectItem(-1,"-- Seleziona un elemento --" ));
		itemYesNo.add(new SelectItem(1, "Si"));
		itemYesNo.add(new SelectItem(0, "No"));
		return itemYesNo;
	}


}
