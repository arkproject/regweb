package it.arkproject.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.Beneficiassistenziali;
import it.arkproject.model.Demografica;
import it.arkproject.model.Escepropriaabitazione;

import java.util.Iterator;

/**
 * Backing bean for Beneficiassistenziali entities.
 * <p>
 * This class provides CRUD functionality for all Beneficiassistenziali entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class BeneficiassistenzialiBean implements Serializable
{

   private static final long serialVersionUID = 1L;

   /*
    * Support creating and retrieving Beneficiassistenziali entities
    */

   private Integer id;

 	public Integer getId() {
 		return this.id;
 	}

 	public void setId(Integer id) {
 		this.id = id;
 	}

 	private Beneficiassistenziali beneficiassistenziali;


	public Beneficiassistenziali getBeneficiassistenziali() {
		return beneficiassistenziali;
	}

	public void setBeneficiassistenziali(Beneficiassistenziali beneficiassistenziali) {
		this.beneficiassistenziali = beneficiassistenziali;
	}

	@Inject
 	private Conversation conversation;

 	public Conversation getConversation() {
 		return conversation;
 	}

 	public void setConversation(Conversation conversation) {
 		this.conversation = conversation;
 	}
 	
 	private Integer pazienteId; 

 	public Integer getPazienteId() {
 		return pazienteId;
 	}

 	public void setPazienteId(Integer pazienteId) {
 		this.pazienteId = pazienteId;
 	}
 	
 	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
 	private EntityManager entityManager;


 	/*
 	 * Support listing and POSTing back Indirizzoresidenza entities (e.g. from
 	 * inside an HtmlSelectOneMenu)
 	 */

 	public List<Beneficiassistenziali> getAll() {

 		CriteriaQuery<Beneficiassistenziali> criteria = this.entityManager
 				.getCriteriaBuilder().createQuery(Beneficiassistenziali.class);
 		return this.entityManager.createQuery(
 				criteria.select(criteria.from(Beneficiassistenziali.class)))
 				.getResultList();
 	}

   /*
    * Support adding children to bidirectional, one-to-many tables
    */

   private Beneficiassistenziali add = new Beneficiassistenziali();

   public Beneficiassistenziali getAdd()
   {
      return this.add;
   }

   public Beneficiassistenziali getAdded()
   {
      Beneficiassistenziali added = this.add;
      this.add = new Beneficiassistenziali();
      return added;
   }
}