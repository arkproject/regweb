package it.arkproject.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.Indirizzoresidenza;
import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Comune;
import it.arkproject.util.JsfUtil;


@Named
@Stateful
//@ConversationScoped
@RequestScoped
public class testViewScopeBean {
	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Indirizzoresidenza entities
	 */

	private Integer id;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private Indirizzoresidenza indirizzoresidenza;

	public Indirizzoresidenza getIndirizzoresidenza() {
		return this.indirizzoresidenza;
	}

	public void setIndirizzoresidenza(Indirizzoresidenza indirizzoresidenza) {
		this.indirizzoresidenza = indirizzoresidenza;
	}

	@Inject
	private Conversation conversation;

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}
	
	private Integer pazienteId; 

	public Integer getPazienteId() {
		return pazienteId;
	}

	public void setPazienteId(Integer pazienteId) {
		this.pazienteId = pazienteId;
	}
	
	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		if(facesContext.getExternalContext().getRequestParameterMap().get("pazienteId") != null){
			pazienteId = Integer.parseInt(facesContext.getExternalContext()
					.getRequestParameterMap().get("pazienteId"));
		}
		
//		this.conversation.begin();
//		this.conversation.setTimeout(1800000L);
		this.id = null;
		indirizzoresidenza = new Indirizzoresidenza();
		return "/pages/secure/paziente/anagrafica/schede/indirizzoTestViewScope?faces-redirect=true";
		

	}

	public void retrieve() {

//		Integer id = null;
//		
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		if(facesContext.getExternalContext().getRequestParameterMap().get("id") != null){
//			id = Integer.parseInt(facesContext.getExternalContext()
//					.getRequestParameterMap().get("id"));
//		}

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (id != null) {
			this.indirizzoresidenza = findById(getId());
		}
		
	}

	public Indirizzoresidenza findById(Integer id) {
		return this.entityManager.find(Indirizzoresidenza.class, id);
	}

	/*
	 * Support updating and deleting Indirizzoresidenza entities
	 */

	public String update() {
		//this.conversation.end();

		try {
			if (this.id == null) {
				
				this.indirizzoresidenza.setAnagraficapaziente(new Anagraficapaziente(pazienteId));
				this.entityManager.persist(this.indirizzoresidenza);
				JsfUtil.addSuccessMessageDettagli(
						JsfUtil.EntitaNomeScheda.Indirizzoresidenza.getText(),
						ResourceBundle.getBundle("propertiesFile/Bundle")
								.getString("IndirizzoresidenzaCreated"));
				return "/pages/secure/paziente/patientDashboard";
			} else {
				this.entityManager.merge(this.indirizzoresidenza);
				JsfUtil.addSuccessMessageDettagli(
						JsfUtil.EntitaNomeScheda.Indirizzoresidenza.getText(),
						ResourceBundle.getBundle("propertiesFile/Bundle")
								.getString("IndirizzoresidenzaUpdated"));
				//return null;
				return  "/pages/secure/paziente/patientDashboard";
				// return "/pages/secure/paziente/patientDashboard?&id="
				// + this.anagraficapaziente.getPazienteId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Indirizzoresidenza deletableEntity = findById(getId());
			Anagraficapaziente anagraficapaziente = deletableEntity
					.getAnagraficapaziente();
			anagraficapaziente.getIndirizzoresidenzas().remove(deletableEntity);
			deletableEntity.setAnagraficapaziente(null);
			this.entityManager.merge(anagraficapaziente);
			Comune comune = deletableEntity.getComune();
			comune.getIndirizzoresidenzas().remove(deletableEntity);
			deletableEntity.setComune(null);
			this.entityManager.merge(comune);
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support listing and POSTing back Indirizzoresidenza entities (e.g. from
	 * inside an HtmlSelectOneMenu)
	 */

	public List<Indirizzoresidenza> getAll() {

		CriteriaQuery<Indirizzoresidenza> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Indirizzoresidenza.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(Indirizzoresidenza.class)))
				.getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Indirizzoresidenza add = new Indirizzoresidenza();

	public Indirizzoresidenza getAdd() {
		return this.add;
	}

	public Indirizzoresidenza getAdded() {
		Indirizzoresidenza added = this.add;
		this.add = new Indirizzoresidenza();
		return added;
	}

}
