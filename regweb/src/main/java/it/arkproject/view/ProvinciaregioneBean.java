package it.arkproject.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Comune;
import it.arkproject.model.Eventopaziente;
import it.arkproject.model.Provinciaregione;

/**
 * Backing bean for Provinciaregione entities.
 * <p>
 * This class provides CRUD functionality for all Provinciaregione entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */
  
@Named
@Stateful
@ConversationScoped
public class ProvinciaregioneBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Provinciaregione entities
	 */
	
  @PostConstruct
  public void init() {
		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}
  	
      this.provinciaregione = new Provinciaregione();
  }
	

	private Integer id;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private Provinciaregione provinciaregione;

	public Provinciaregione getProvinciaregione() {
		return this.provinciaregione;
	}

	public void setProvinciaregione(Provinciaregione provinciaregione) {
		this.provinciaregione = provinciaregione;
	}

	private List<Comune> comuni;

	public List<Comune> getComuni() {
		return comuni;
	}

	public void setComuni(List<Comune> comuni) {
		this.comuni = comuni;
	}

	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public Provinciaregione findById(Integer id) {

		return this.entityManager.find(Provinciaregione.class, id);
	}

	private Predicate[] getSearchPredicates(Root<Comune> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		Provinciaregione provinciaregione = this.provinciaregione;
		if (provinciaregione != null) {
			predicatesList.add(builder.equal(root.get("provinciaregione"),
					provinciaregione));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Provinciaregione> getAll() {
		// **** VERSIONE RIDOTTA
		// CriteriaQuery<Provinciaregione> criteria = this.entityManager
		// .getCriteriaBuilder().createQuery(Provinciaregione.class);
		// return this.entityManager.createQuery(
		// criteria.select(criteria.from(Provinciaregione.class)))
		// .getResultList();
		// **** FINE

		CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Provinciaregione> cq = cb
				.createQuery(Provinciaregione.class);
		Root<Provinciaregione> root = cq.from(Provinciaregione.class);
		cq.select(root);
		cq.orderBy(cb.asc(root.get("denominazioneProvincia")));

		TypedQuery<Provinciaregione> query = this.entityManager.createQuery(cq);
		return query.getResultList();

	}

	public void getComuniByProvincia() {

		// **** ORDERBY RALLENTA MOLTO LE PRESTAZIONI
		// CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		// CriteriaQuery<Comune> cq = cb
		// .createQuery(Comune.class);
		// Root<Comune> root = cq.from(Comune.class);
		// cq.select(root);
		// cq.orderBy(cb.asc(root.get("denominazione")));
		//
		// TypedQuery<Comune> query = this.entityManager.createQuery(cq);
		// this.comuni = query.getResultList();

		// this.comuni = null;

		if (provinciaregione != null) {
			CriteriaQuery<Comune> criteria = this.entityManager
					.getCriteriaBuilder().createQuery(Comune.class);

			Root<Comune> root = criteria.from(Comune.class);
			TypedQuery<Comune> query = this.entityManager.createQuery(criteria
					.select(root).where(getSearchPredicates(root)));

			this.comuni = query.getResultList();
		} else {
			this.comuni = null;
		}
	}

	public void preloadComuneByProvincia(Integer idProvincia) {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}
		
		
		if (idProvincia == null) {
			this.comuni = null;
		} else {
			CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
			CriteriaQuery<Comune> criteria = this.entityManager
					.getCriteriaBuilder().createQuery(Comune.class);

			Root<Comune> root = criteria.from(Comune.class);

			List<Predicate> predicatesList = new ArrayList<Predicate>();
			if (idProvincia != null) {
				predicatesList.add(cb.equal(root.get("provinciaregione"),
						idProvincia));
			}

			TypedQuery<Comune> query = this.entityManager.createQuery(criteria
					.select(root).where(
							predicatesList.toArray(new Predicate[predicatesList
									.size()])));
			this.comuni = query.getResultList();
		}

	}

}