package it.arkproject.view;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaQuery;

import it.arkproject.model.Istituto;

@Named
@Stateful
@SessionScoped
public class IstitutoBean implements Serializable
{
   private static final long serialVersionUID = 1L;

   private Istituto current;
   

   @PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public List<Istituto> getAll() {
		CriteriaQuery<Istituto> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Istituto.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(Istituto.class)))
				.getResultList();
	}
	
	public void  findById (Integer id){
		current = this.entityManager.find(Istituto.class, id);
	}

	public Istituto getCurrent() {
		return current;
	}

	public void setCurrent(Istituto current) {
		this.current = current;
	}
	

}