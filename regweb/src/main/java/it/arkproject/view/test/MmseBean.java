package it.arkproject.view.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import it.arkproject.genericClass.AbstractBean;
import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Mmse;
import it.arkproject.view.AnagraficaPazienteBean;

@Named
@ConversationScoped
public class MmseBean extends AbstractBean<Mmse> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	AnagraficaPazienteBean anagraficaPazienteBean;

	private Date dataCompilazione = null;

	public Integer somma() {
		Integer score = 0;

		if (this.getCurrent().getInCheAnnoSiamo() != null) {
			score = this.getCurrent().getInCheAnnoSiamo();
		}
		if (this.getCurrent().getInCheMeseSiamo() != null) {
			score = score + this.getCurrent().getInCheMeseSiamo();
		}
		if (this.getCurrent().getAchePianoSiamo() != null) {
			score = score + this.getCurrent().getAchePianoSiamo();
		}
		if (this.getCurrent().getGiornoSettimanaOggi() != null) {
			score = score + this.getCurrent().getGiornoSettimanaOggi();
		}
		if (this.getCurrent().getInQualeRegioneSiamo() != null) {
			score = score + this.getCurrent().getInQualeRegioneSiamo();
		}
		if (this.getCurrent().getNomeDelLuogo() != null) {
			score = score + this.getCurrent().getNomeDelLuogo();
		}
		if (this.getCurrent().getRipetereCasaPaneGatto() != null) {
			score = score + this.getCurrent().getRipetereCasaPaneGatto();
		}
		if (this.getCurrent().getScrivereFrase() != null) {
			score = score + this.getCurrent().getScrivereFrase();
		}
		if (this.getCurrent().getMostrareOrologiaMatita() != null) {
			score = score + this.getCurrent().getMostrareOrologiaMatita();
		}
		if (this.getCurrent().getLeggereEdEseguire() != null) {
			score = score + this.getCurrent().getLeggereEdEseguire();
		}
		if (this.getCurrent().getInCheStagioneSiamo() != null) {
			score = score + this.getCurrent().getInCheStagioneSiamo();
		}
		if (this.getCurrent().getDataOdierna() != null) {
			score = score + this.getCurrent().getDataOdierna();
		}
		if (this.getCurrent().getInQualeCittaCiTroviamo() != null) {
			score = score + this.getCurrent().getInQualeCittaCiTroviamo();
		}
		if (this.getCurrent().getInCheNazioneSiamo() != null) {
			score = score + this.getCurrent().getInCheNazioneSiamo();
		}
		if (this.getCurrent().getContareRitroso() != null) {
			score = score + this.getCurrent().getContareRitroso();
		}
		if (this.getCurrent().getRipetereNomeOggettiPrecti() != null) {
			score = score + this.getCurrent().getRipetereNomeOggettiPrecti();
		}
		if (this.getCurrent().getRipetereTigreControTigre() != null) {
			score = score + this.getCurrent().getRipetereTigreControTigre();
		}
		if (this.getCurrent().getCopiareDisegno() != null) {
			score = score + this.getCurrent().getCopiareDisegno();
		}
		if (this.getCurrent().getPiegareFoglio() != null) {
			score = score + this.getCurrent().getPiegareFoglio();
		}

		if (score > 0) {
			if (this.dataCompilazione != null) {
				this.getCurrent()
						.setScoreAdjustment(
								calcolaScoreAdjustment(
										score,
										this.anagraficaPazienteBean
												.getAnagraficapaziente()
												.getScolarita(),
										calcoloEta(
												this.dataCompilazione,
												this.anagraficaPazienteBean
														.getAnagraficapaziente()
														.getDataNascita())));
				System.out.println(score
						+ " "
						+ this.anagraficaPazienteBean.getAnagraficapaziente()
								.getScolarita()
						+ " "
						+ calcoloEta(this.dataCompilazione,
								this.anagraficaPazienteBean
										.getAnagraficapaziente()
										.getDataNascita()));
			}
			this.getCurrent().setScore(score);
		} else {
			this.getCurrent().setScore(0);
			this.getCurrent().setScoreAdjustment(0.0);
		}

		return score;

	}

	// Calcolo l'età del paziente
	public int calcoloEta(Date dataCompilazioneMMSE, Date dataNascitaAnagrafica) {
		Calendar dataCompilazione = Calendar.getInstance();
		Calendar dataNascita = Calendar.getInstance();

		dataCompilazione.setTime(dataCompilazioneMMSE);
		dataNascita.setTime(dataNascitaAnagrafica);

		return dataCompilazione.get(Calendar.YEAR)
				- dataNascita.get(Calendar.YEAR);
	}

	public double calcolaScoreAdjustment(int score, int scolarita, int eta) {
		double ScoreAdjustment = 0;
		if ((eta >= 65) && (eta <= 69)) {
			if ((scolarita >= 0) && (scolarita <= 4)) {
				ScoreAdjustment = (double) score + 0.4;
			} else if ((scolarita >= 5) && (scolarita <= 7)) {
				ScoreAdjustment = (double) score - 1.1;
			} else if ((scolarita >= 8) && (scolarita <= 12)) {
				ScoreAdjustment = (double) score - 2.0;
			} else if ((scolarita >= 13) && (scolarita <= 17)) {
				ScoreAdjustment = (double) score - 2.8;
			}

		} else if ((eta >= 70) && (eta <= 74)) {
			if ((scolarita >= 0) && (scolarita <= 4)) {
				ScoreAdjustment = (double) score + 0.7;
			} else if ((scolarita >= 5) && (scolarita <= 7)) {
				ScoreAdjustment = (double) score - 0.7;
			} else if ((scolarita >= 8) && (scolarita <= 12)) {
				ScoreAdjustment = (double) score - 1.6;
			} else if ((scolarita >= 13) && (scolarita <= 17)) {
				ScoreAdjustment = (double) score - 2.3;
			}
		}

		else if ((eta >= 75) && (eta <= 79)) {
			if ((scolarita >= 0) && (scolarita <= 4)) {
				ScoreAdjustment = (double) score + 1.0;
			} else if ((scolarita >= 5) && (scolarita <= 7)) {
				ScoreAdjustment = (double) score - 0.3;
			} else if ((scolarita >= 8) && (scolarita <= 12)) {
				ScoreAdjustment = (double) score - 1.0;
			} else if ((scolarita >= 13) && (scolarita <= 17)) {
				ScoreAdjustment = (double) score - 1.7;
			}
		}
		
		else if ((eta >= 80) && (eta <= 84)) {
			if ((scolarita >= 0) && (scolarita <= 4)) {
				ScoreAdjustment = (double) score + 1.5;
			} else if ((scolarita >= 5) && (scolarita <= 7)) {
				ScoreAdjustment = (double) score + 0.4;
			} else if ((scolarita >= 8) && (scolarita <= 12)) {
				ScoreAdjustment = (double) score - 0.3;
			} else if ((scolarita >= 13) && (scolarita <= 17)) {
				ScoreAdjustment = (double) score - 0.9;
			}
		}
		
		else if ((eta >= 85) && (eta <= 89)) {
			if ((scolarita >= 0) && (scolarita <= 4)) {
				ScoreAdjustment = (double) score + 2.2;
			} else if ((scolarita >= 5) && (scolarita <= 7)) {
				ScoreAdjustment = (double) score + 1.4;
			} else if ((scolarita >= 8) && (scolarita <= 12)) {
				ScoreAdjustment = (double) score + 0.8;
			} else if ((scolarita >= 13) && (scolarita <= 17)) {
				ScoreAdjustment = (double) score + 0.3;
			}
		}

		else {
			ScoreAdjustment = score;
		}
		return ScoreAdjustment;
	}

	public void onDateSelect(SelectEvent event) {
		this.dataCompilazione = (Date) event.getObject();
	}

};
