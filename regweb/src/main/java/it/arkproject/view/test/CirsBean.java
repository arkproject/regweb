package it.arkproject.view.test;

import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Cirs;
import it.arkproject.model.Mmse;
import it.arkproject.util.JsfUtil;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaQuery;

@Named
@Stateful
@ConversationScoped
public class CirsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Mmse entities
	 */

	private Integer id;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private Cirs current;

	public Cirs getCurrent() {
		return current;
	}

	public void setCurrent(Cirs current) {
		this.current = current;
	}

	@Inject
	private Conversation conversation;

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}
	
	private Integer pazienteId; 

	public Integer getPazienteId() {
		return pazienteId;
	}

	public void setPazienteId(Integer pazienteId) {
		this.pazienteId = pazienteId;
	}
	
	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		if(facesContext.getExternalContext().getRequestParameterMap().get("pazienteId") != null){
			pazienteId = Integer.parseInt(facesContext.getExternalContext()
					.getRequestParameterMap().get("pazienteId"));
		}
		
//		this.conversation.begin();
//		this.conversation.setTimeout(1800000L);
		this.id = null;
		current = new Cirs();
		return "/pages/secure/paziente/test/cirs?faces-redirect=true";
		

	}

	public void retrieve() {

//		Integer id = null;
//		
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		if(facesContext.getExternalContext().getRequestParameterMap().get("id") != null){
//			id = Integer.parseInt(facesContext.getExternalContext()
//					.getRequestParameterMap().get("id"));
//		}

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (id != null) {
			this.current = findById(getId());
		}
		
	}

	public Cirs findById(Integer id) {
		return this.entityManager.find(Cirs.class, id);
	}

	/*
	 * Support updating and deleting Indirizzoresidenza entities
	 */

	public String update() {
		//this.conversation.end();

		try {
			if (this.id == null) {
				
				this.current.setAnagraficapaziente(new Anagraficapaziente(pazienteId));
				this.entityManager.persist(this.current);
				JsfUtil.addSuccessMessageDettagli(
						JsfUtil.EntitaNomeScheda.Cirs.getText(),"La scheda" + JsfUtil.EntitaNomeScheda.Cirs.getText() + "e' stata correttamente creata.");
				return "/pages/secure/paziente/patientDashboard";
			} else {
				this.entityManager.merge(this.current);
				JsfUtil.addSuccessMessageDettagli(
						JsfUtil.EntitaNomeScheda.Cirs.getText(),
						"La scheda" + JsfUtil.EntitaNomeScheda.Cirs.getText() + "e' stata correttamente aggiornata.");
				return  "/pages/secure/paziente/patientDashboard";
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}


	/*
	 * Support listing and POSTing back Indirizzoresidenza entities (e.g. from
	 * inside an HtmlSelectOneMenu)
	 */

	public List<Cirs> getAll() {

		CriteriaQuery<Cirs> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Cirs.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(Cirs.class)))
				.getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Cirs add = new Cirs();

	public Cirs getAdd() {
		return this.add;
	}

	public Cirs getAdded() {
		Cirs added = this.add;
		this.add = new Cirs();
		return added;
	}
}
