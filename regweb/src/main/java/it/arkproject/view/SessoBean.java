package it.arkproject.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.Sesso;
import it.arkproject.model.Anagraficapaziente;
import java.util.Iterator;

/**
 * Backing bean for Sesso entities.
 * <p>
 * This class provides CRUD functionality for all Sesso entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class SessoBean implements Serializable
{

   private static final long serialVersionUID = 1L;

   /*
    * Support creating and retrieving Sesso entities
    */

   private Integer id;

   public Integer getId()
   {
      return this.id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   private Sesso sesso;

   public Sesso getSesso()
   {
      return this.sesso;
   }

   public void setSesso(Sesso sesso)
   {
      this.sesso = sesso;
   }

   @Inject
   private Conversation conversation;

   @PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
   private EntityManager entityManager;

   public String create()
   {

      this.conversation.begin();
      this.conversation.setTimeout(1800000L);
      return "create?faces-redirect=true";
   }

   public void retrieve()
   {

      if (FacesContext.getCurrentInstance().isPostback())
      {
         return;
      }

      if (this.conversation.isTransient())
      {
         this.conversation.begin();
         this.conversation.setTimeout(1800000L);
      }

      if (this.id == null)
      {
         this.sesso = this.example;
      }
      else
      {
         this.sesso = findById(getId());
      }
   }

   public Sesso findById(Integer id)
   {

      return this.entityManager.find(Sesso.class, id);
   }

   /*
    * Support updating and deleting Sesso entities
    */

   public String update()
   {
      this.conversation.end();

      try
      {
         if (this.id == null)
         {
            this.entityManager.persist(this.sesso);
            return "search?faces-redirect=true";
         }
         else
         {
            this.entityManager.merge(this.sesso);
            return "view?faces-redirect=true&id=" + this.sesso.getSessoId();
         }
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   public String delete()
   {
      this.conversation.end();

      try
      {
         Sesso deletableEntity = findById(getId());
         Iterator<Anagraficapaziente> iterAnagraficapazientes = deletableEntity.getAnagraficapazientes().iterator();
         for (; iterAnagraficapazientes.hasNext();)
         {
            Anagraficapaziente nextInAnagraficapazientes = iterAnagraficapazientes.next();
            nextInAnagraficapazientes.setSesso(null);
            iterAnagraficapazientes.remove();
            this.entityManager.merge(nextInAnagraficapazientes);
         }
         this.entityManager.remove(deletableEntity);
         this.entityManager.flush();
         return "search?faces-redirect=true";
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   /*
    * Support searching Sesso entities with pagination
    */

   private int page;
   private long count;
   private List<Sesso> pageItems;

   private Sesso example = new Sesso();

   public int getPage()
   {
      return this.page;
   }

   public void setPage(int page)
   {
      this.page = page;
   }

   public int getPageSize()
   {
      return 10;
   }

   public Sesso getExample()
   {
      return this.example;
   }

   public void setExample(Sesso example)
   {
      this.example = example;
   }

   public void search()
   {
      this.page = 0;
   }

   public void paginate()
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

      // Populate this.count

      CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
      Root<Sesso> root = countCriteria.from(Sesso.class);
      countCriteria = countCriteria.select(builder.count(root)).where(
            getSearchPredicates(root));
      this.count = this.entityManager.createQuery(countCriteria)
            .getSingleResult();

      // Populate this.pageItems

      CriteriaQuery<Sesso> criteria = builder.createQuery(Sesso.class);
      root = criteria.from(Sesso.class);
      TypedQuery<Sesso> query = this.entityManager.createQuery(criteria
            .select(root).where(getSearchPredicates(root)));
      query.setFirstResult(this.page * getPageSize()).setMaxResults(
            getPageSize());
      this.pageItems = query.getResultList();
   }

   private Predicate[] getSearchPredicates(Root<Sesso> root)
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
      List<Predicate> predicatesList = new ArrayList<Predicate>();

      String descrizione = this.example.getDescrizione();
      if (descrizione != null && !"".equals(descrizione))
      {
         predicatesList.add(builder.like(builder.lower(root.<String> get("descrizione")), '%' + descrizione.toLowerCase() + '%'));
      }

      return predicatesList.toArray(new Predicate[predicatesList.size()]);
   }

   public List<Sesso> getPageItems()
   {
      return this.pageItems;
   }

   public long getCount()
   {
      return this.count;
   }

   /*
    * Support listing and POSTing back Sesso entities (e.g. from inside an
    * HtmlSelectOneMenu)
    */

   public List<Sesso> getAll()
   {

      CriteriaQuery<Sesso> criteria = this.entityManager
            .getCriteriaBuilder().createQuery(Sesso.class);
      return this.entityManager.createQuery(
            criteria.select(criteria.from(Sesso.class))).getResultList();
   }

   @Resource
   private SessionContext sessionContext;

   public Converter getConverter()
   {

      final SessoBean ejbProxy = this.sessionContext.getBusinessObject(SessoBean.class);

      return new Converter()
      {

         @Override
         public Object getAsObject(FacesContext context,
               UIComponent component, String value)
         {

            return ejbProxy.findById(Integer.valueOf(value));
         }

         @Override
         public String getAsString(FacesContext context,
               UIComponent component, Object value)
         {

            if (value == null)
            {
               return "";
            }

            return String.valueOf(((Sesso) value).getSessoId());
         }
      };
   }

   /*
    * Support adding children to bidirectional, one-to-many tables
    */

   private Sesso add = new Sesso();

   public Sesso getAdd()
   {
      return this.add;
   }

   public Sesso getAdded()
   {
      Sesso added = this.add;
      this.add = new Sesso();
      return added;
   }
}