package it.arkproject.view;

import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.sort;
import static ch.lambdaj.group.Groups.by;
import static ch.lambdaj.group.Groups.group;
import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Comune;
import it.arkproject.model.Demografica;
import it.arkproject.model.EventiPazienteSubTable;
import it.arkproject.model.Eventopaziente;
import it.arkproject.model.Indirizzoresidenza;
import it.arkproject.model.Mmse;
import it.arkproject.model.Sesso;
import it.arkproject.util.CodiceFiscaleGeneratore;
import it.arkproject.util.JsfUtil;
import it.arkproject.util.UCheckDigit;
import it.arkproject.view.test.MmseBean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import ch.lambdaj.group.Group;


  
@Named("anaTestBean")
@Stateful
@ViewScoped
public class AnaTestBean {
	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Anagraficapaziente entities
	 */

	private Anagraficapaziente anagraficapaziente;
	
    @PostConstruct
    public void init() {
		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}
    	
        this.anagraficapaziente = new Anagraficapaziente();
    }
	
	
	@Inject
	MmseBean mmseBean;
	
	private Integer id = 1;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public Anagraficapaziente getAnagraficapaziente() {
		return this.anagraficapaziente;
	}

	public void setAnagraficapaziente(Anagraficapaziente anagraficapaziente) {
		this.anagraficapaziente = anagraficapaziente;
	}

	private Integer idIstituto;

	public Integer getIdIstituto() {
		return idIstituto;
	}

	public void setIdIstituto(Integer idIstituto) {
		this.idIstituto = idIstituto;
	}

//	@Inject
//	private Conversation conversation;
//
//	public Conversation getConversation() {
//		return conversation;
//	}
//
//	public void setConversation(Conversation conversation) {
//		this.conversation = conversation;
//	}

	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {
//		this.conversation.begin();
//		this.conversation.setTimeout(1800000L);
		return "/pages/secure/paziente/anagrafica/create?faces-redirect=true";
	}
	

//	public void endConversation() {
//		if (!this.conversation.isTransient()) {
//			this.conversation.end();
//		}
//	}
   
	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}
  
//		if (this.conversation.isTransient()) {
//			this.conversation.begin();
//			this.conversation.setTimeout(1800000L);
//		}

		if (this.id == null) {
		//	this.anagraficapaziente = this.anaPaziente;
		} else {
			this.anagraficapaziente = findById(getId());
			this.entityManager.detach(this.anagraficapaziente);
		}

		// gruppoSchedeCompilate();
	}
	
   
	public void refreshAnagrafica() {
		this.anagraficapaziente = findById(getId());
	}

	public Anagraficapaziente findById(Integer id) {
		return this.entityManager.find(Anagraficapaziente.class, id);

	}
    
	/*
	 * Support updating and deleting Anagraficapaziente entities
	 */
   
	public String update() {
		
		try {
			if (this.id == null) {

				// Aggiungo idIstituto di appartenenza dell'utente.
				// Ritengo che questo valore non deve essere modificabile in
				// fase di edit.
				this.anagraficapaziente.setIstitutoId(this.idIstituto);
//				this.entityManager.persist(this.anagraficapaziente);
				JsfUtil.addSuccessMessageDettagli(
						JsfUtil.EntitaNomeScheda.Anagraficapaziente.getText(),
						ResourceBundle.getBundle("propertiesFile/Bundle")
								.getString("AnagraficapazienteCreated"));
			} else {
				
//				this.entityManager.merge(this.anagraficapaziente);
//				aggiornaScoreMMSE();
//				this.entityManager.flush();	
//				JsfUtil.addSuccessMessageDettagli(
//						JsfUtil.EntitaNomeScheda.Anagraficapaziente.getText(),
//						ResourceBundle.getBundle("propertiesFile/Bundle")
//								.getString("AnagraficapazienteUpdated"));
			}
			 return "/pages/secure/paziente/patientDashboard?faces-redirect=true";
//			return "/pages/secure/paziente/patientDashboard?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}
	    
	public String delete() {
//		this.conversation.end();

		try {
			Anagraficapaziente deletableEntity = findById(getId());
			Comune comune = deletableEntity.getComune();
			comune.getAnagraficapazientes().remove(deletableEntity);
			deletableEntity.setComune(null);
			this.entityManager.merge(comune);
			Sesso sesso = deletableEntity.getSesso();
			sesso.getAnagraficapazientes().remove(deletableEntity);
			deletableEntity.setSesso(null);
			this.entityManager.merge(sesso);
			Iterator<Demografica> iterDemograficas = deletableEntity
					.getDemograficas().iterator();
			for (; iterDemograficas.hasNext();) {
				Demografica nextInDemograficas = iterDemograficas.next();
				nextInDemograficas.setAnagraficapaziente(null);
				iterDemograficas.remove();
				this.entityManager.merge(nextInDemograficas);
			}
			Iterator<Eventopaziente> iterEventopazientes = deletableEntity
					.getEventopazientes().iterator();
			for (; iterEventopazientes.hasNext();) {
				Eventopaziente nextInEventopazientes = iterEventopazientes
						.next();
				nextInEventopazientes.setAnagraficapaziente(null);
				iterEventopazientes.remove();
				this.entityManager.merge(nextInEventopazientes);
			}
			Iterator<Indirizzoresidenza> iterIndirizzoresidenzas = deletableEntity
					.getIndirizzoresidenzas().iterator();
			for (; iterIndirizzoresidenzas.hasNext();) {
				Indirizzoresidenza nextInIndirizzoresidenzas = iterIndirizzoresidenzas
						.next();
				nextInIndirizzoresidenzas.setAnagraficapaziente(null);
				iterIndirizzoresidenzas.remove();
				this.entityManager.merge(nextInIndirizzoresidenzas);
			}
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}
	 
	
	
	private void  aggiornaScoreMMSE(){
		Iterator<Mmse> iterMmses = this.anagraficapaziente
				.getMmses().iterator();
		for (; iterMmses.hasNext();) {
			Integer eta = null;
			double scoreAdjustment = 0;
			Mmse nextInMmses = iterMmses.next();
			eta= mmseBean.calcoloEta(nextInMmses.getDataCompilazione(), this.anagraficapaziente.getDataNascita());
			
			scoreAdjustment = mmseBean.calcolaScoreAdjustment(nextInMmses.getScore(), this.anagraficapaziente.getScolarita(), eta);

			nextInMmses.setScoreAdjustment(scoreAdjustment);
		//	iterMmses.remove();
			this.entityManager.merge(nextInMmses);
		}		
	}
	
 
	/*
	 * Support searching Anagraficapaziente entities with pagination
	 */

	private int page;
	private long count;
	private List<Anagraficapaziente> pageItems;

	private Anagraficapaziente anaPaziente = new Anagraficapaziente();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Anagraficapaziente getAnaPaziente() {
		return anaPaziente;
	}

	public void setAnaPaziente(Anagraficapaziente anaPaziente) {
		this.anaPaziente = anaPaziente;
	}

	public void search() {
		this.page = 0;
	}

	public void paginate(Integer idIstituto, boolean isUserAdmin) {

		if (!isUserAdmin) {
			this.idIstituto = idIstituto;
		}
		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Anagraficapaziente> root = countCriteria
				.from(Anagraficapaziente.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<Anagraficapaziente> criteria = builder
				.createQuery(Anagraficapaziente.class);
		root = criteria.from(Anagraficapaziente.class);
		TypedQuery<Anagraficapaziente> query = this.entityManager
				.createQuery(criteria.select(root).where(
						getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(
				getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Anagraficapaziente> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		if (this.idIstituto != null) {
			predicatesList.add(builder.equal(root.get("istitutoId"),
					this.idIstituto));
		}
		Comune comune = this.anaPaziente.getComune();
		if (comune != null) {
			predicatesList.add(builder.equal(root.get("comune"), comune));
		}
		Sesso sesso = this.anaPaziente.getSesso();
		if (sesso != null) {
			predicatesList.add(builder.equal(root.get("sesso"), sesso));
		}
		String cognome = this.anaPaziente.getCognome();
		if (cognome != null && !"".equals(cognome)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("cognome")),
					'%' + cognome.toLowerCase() + '%'));
		}
		String nome = this.anaPaziente.getNome();
		if (nome != null && !"".equals(nome)) {
			predicatesList.add(builder.like(
					builder.lower(root.<String> get("nome")),
					'%' + nome.toLowerCase() + '%'));
		}
		String codiceFiscale = this.anaPaziente.getCodiceFiscale();
		if (codiceFiscale != null && !"".equals(codiceFiscale)) {
			predicatesList.add(builder.equal(
					root.<String> get("codiceFiscale"), codiceFiscale));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Anagraficapaziente> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	public void getCountbyIstituto() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Anagraficapaziente> root = countCriteria
				.from(Anagraficapaziente.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();
	}

	public void getCountbyIstituto(Integer idIstituto) {

		if (idIstituto == null) {
			getCountbyIstituto();
			return;
		}

		this.idIstituto = idIstituto;

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Anagraficapaziente> root = countCriteria
				.from(Anagraficapaziente.class);
		countCriteria = countCriteria.select(builder.count(root)).where(
				builder.equal(root.<Integer> get("istitutoId"), idIstituto));
		this.count = this.entityManager.createQuery(countCriteria)
				.getSingleResult();
	}

	/*
	 * Support listing and POSTing back Anagraficapaziente entities (e.g. from
	 * inside an HtmlSelectOneMenu)
	 */

	public List<Anagraficapaziente> getAll() {

		CriteriaQuery<Anagraficapaziente> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Anagraficapaziente.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(Anagraficapaziente.class)))
				.getResultList();
	}

	private Boolean checkCodiceFiscaleUnique(String cf) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Anagraficapaziente> root = countCriteria
				.from(Anagraficapaziente.class);

		List<Predicate> predicatesList = new ArrayList<Predicate>();
		if (this.id != null) {
			predicatesList.add(builder.equal(root.get("pazienteId"), this.id));
		}
		if (cf != null) {
			predicatesList.add(builder.equal(root.get("codiceFiscale"), cf));
		}

		countCriteria = countCriteria.select(builder.count(root)).where(
				predicatesList.toArray(new Predicate[predicatesList.size()]));

		// Populate this.count

		// CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		// Root<Anagraficapaziente> root = countCriteria
		// .from(Anagraficapaziente.class);
		// countCriteria = countCriteria.select(builder.count(root)).where(
		// builder.equal(root.<String> get("codiceFiscale"),cf));
		if (this.entityManager.createQuery(countCriteria).getSingleResult()
				.byteValue() > 1) {
			return false;
		} else {
			return true;
		}

	}

	@Resource
	private SessionContext sessionContext;

	public Converter getConverter() {

		final AnagraficaPazienteBean ejbProxy = this.sessionContext
				.getBusinessObject(AnagraficaPazienteBean.class);

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context,
					UIComponent component, String value) {

				return ejbProxy.findById(Integer.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context,
					UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Anagraficapaziente) value)
						.getPazienteId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Anagraficapaziente add = new Anagraficapaziente();

	public Anagraficapaziente getAdd() {
		return this.add;
	}

	public Anagraficapaziente getAdded() {
		Anagraficapaziente added = this.add;
		this.add = new Anagraficapaziente();
		return added;
	}

	public void codiceFiscale(ActionEvent event) {

		// FacesContext.getCurrentInstance().getMessages().remove();

		// Questo può essere utile se occorre avere il valore direttamente
		// dal component
		// String value = FacesContext.getCurrentInstance().
		// getExternalContext().getRequestParameterMap().get("cc");
		// this.anagraficapaziente.setCodiceFiscale(null);
		// RequestContext.getCurrentInstance().reset("form:codiceFiscale");

		if ((anagraficapaziente.getCognome() != null)
				&& (anagraficapaziente.getNome() != null)
				&& (anagraficapaziente.getSesso() != null)
				&& (anagraficapaziente.getDataNascita() != null)
				&& (anagraficapaziente.getComune() != null)) {

			String cognomeCF = anagraficapaziente.getCognome();
			String nomeCF = anagraficapaziente.getNome();
			String sexCF = ("Maschio".equals(anagraficapaziente.getSesso()
					.getDescrizione()) ? "M" : "F");
			String codiceCatastaleCF = anagraficapaziente.getComune()
					.getCodiceCatastale();

			Calendar cal = Calendar.getInstance();
			cal.setTime(anagraficapaziente.getDataNascita());

			String cf = CodiceFiscaleGeneratore.calcolaCodiceFiscaleGeneratore(
					nomeCF, cognomeCF, cal.get(Calendar.DAY_OF_MONTH),
					(cal.get(Calendar.MONTH) + 1), cal.get(Calendar.YEAR),
					sexCF, codiceCatastaleCF);

			anagraficapaziente.setCodiceFiscale(cf);
		}
	}

	public void validateCodiceFiscale(ComponentSystemEvent event) {
		FacesContext fc = FacesContext.getCurrentInstance();
		UIComponent components = event.getComponent();

		// get codiceFiscale
		UIInput uiInputCodiceFiscale = (UIInput) components
				.findComponent("codiceFiscale");
		String cf = uiInputCodiceFiscale.getLocalValue() == null ? ""
				: uiInputCodiceFiscale.getLocalValue().toString();
		String codiceFiscaleId = uiInputCodiceFiscale.getClientId();

		if (cf.length() == 16) {
			UCheckDigit checkDigit = new UCheckDigit(cf);
			if (!checkDigit.controllaCorrettezza()) {
				FacesMessage msg = new FacesMessage("Codice fiscale errato");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				fc.addMessage(codiceFiscaleId, msg);
				fc.renderResponse();
			}

			if (checkCodiceFiscaleUnique(cf) == false) {
				FacesMessage msg = new FacesMessage(
						"Codice fiscale già presente in anagrafica");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				fc.addMessage(codiceFiscaleId, msg);
				fc.renderResponse();

			}
		}
	}

	List<EventiPazienteSubTable> sorted;

	public List<EventiPazienteSubTable> getSorted() {
		return sorted;
	}

	public void setSorted(List<EventiPazienteSubTable> sorted) {
		this.sorted = sorted;
	}

	public void gruppoSchedeCompilate() {

		// if (FacesContext.getCurrentInstance().isPostback()) {
		// return sorted;
		// }

		// if (sorted == null) {

		List<EventiPazienteSubTable> eventiPazientiSubTables;

		eventiPazientiSubTables = new ArrayList<>();

		Group<Eventopaziente> groupTest = group(
				this.anagraficapaziente.getEventopazientes(),
				by(on(Eventopaziente.class).getDescrizione()));

		Set<String> groupTestKeys = groupTest.keySet();

		for (String ageKey : groupTestKeys) {
			EventiPazienteSubTable subTable = new EventiPazienteSubTable(ageKey);
			for (Eventopaziente eventopaziente : groupTest.find(ageKey)) {
				subTable.getEventoPazientes().add(eventopaziente);
			}
			eventiPazientiSubTables.add(subTable);
		}
		this.sorted = sort(eventiPazientiSubTables,
				on(EventiPazienteSubTable.class).getNomeScheda());
		//
		// }
		// return sorted;
	}

	public List<EventiPazienteSubTable> gruppoSchedeCompilate2() {

		// if (FacesContext.getCurrentInstance().isPostback()) {
		// return sorted;
		// }

		// if (sorted == null) {

		List<EventiPazienteSubTable> eventiPazientiSubTables;

		eventiPazientiSubTables = new ArrayList<>();

		Group<Eventopaziente> groupTest = group(
				this.anagraficapaziente.getEventopazientes(),
				by(on(Eventopaziente.class).getDescrizione()));

		Set<String> groupTestKeys = groupTest.keySet();

		for (String ageKey : groupTestKeys) {
			EventiPazienteSubTable subTable = new EventiPazienteSubTable(ageKey);
			for (Eventopaziente eventopaziente : groupTest.find(ageKey)) {
				subTable.getEventoPazientes().add(eventopaziente);
			}
			eventiPazientiSubTables.add(subTable);
		}
		return sort(eventiPazientiSubTables, on(EventiPazienteSubTable.class)
				.getNomeScheda());
		//
		// }
		// return sorted;
	}

}
