package it.arkproject.view;

import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.sort;
import static ch.lambdaj.group.Groups.by;
import static ch.lambdaj.group.Groups.group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ch.lambdaj.group.Group;
import it.arkproject.model.EventiPazienteSubTable;
import it.arkproject.model.Eventopaziente;


@Named
@Stateful
@RequestScoped
public class EventopazienteBean implements Serializable {

	private static final long serialVersionUID = 1L;


	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public List<EventiPazienteSubTable> sortedSubTable;

	public List<EventiPazienteSubTable> getSortedSubTable() {
		return sortedSubTable;
	}

	public void setSortedSubTable(List<EventiPazienteSubTable> sortedSubTable) {
		this.sortedSubTable = sortedSubTable;
	}

	public void gruppoSchedeCompilate(Integer pazienteID) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Eventopaziente> criteria = builder
				.createQuery(Eventopaziente.class);
		Root<Eventopaziente> root = criteria.from(Eventopaziente.class);
		criteria.select(root).where(builder.equal(root.get("anagraficapaziente"),pazienteID))
								.orderBy(builder.asc(root.get("dataCompilazione")));

		TypedQuery<Eventopaziente> query = this.entityManager.createQuery(criteria);

		List<EventiPazienteSubTable> eventiPazientiSubTables;

		eventiPazientiSubTables = new ArrayList<>();

		Group<Eventopaziente> groupTest = group(query.getResultList(),
				by(on(Eventopaziente.class).getDescrizione()));

		Set<String> groupTestKeys = groupTest.keySet();

		for (String ageKey : groupTestKeys) {
			EventiPazienteSubTable subTable = new EventiPazienteSubTable(ageKey);
			for (Eventopaziente eventopaziente : groupTest.find(ageKey)) {
				subTable.getEventoPazientes().add(eventopaziente);
			}
			eventiPazientiSubTables.add(subTable);
		}
		this.sortedSubTable = sort(eventiPazientiSubTables,
				on(EventiPazienteSubTable.class).getNomeScheda());

	}

}