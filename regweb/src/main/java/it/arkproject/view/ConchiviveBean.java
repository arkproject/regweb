package it.arkproject.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Comune;
import it.arkproject.model.Conchivive;
import it.arkproject.model.Demografica;
import it.arkproject.model.Indirizzoresidenza;
import it.arkproject.util.JsfUtil;

import java.util.Iterator;

/**
 * Backing bean for Conchivive entities.
 * <p>
 * This class provides CRUD functionality for all Conchivive entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ConchiviveBean implements Serializable
{

   private static final long serialVersionUID = 1L;

   /*
    * Support creating and retrieving Conchivive entities
    */

   private Integer id;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private Conchivive conchivive;

	public Conchivive getConchivive() {
		return conchivive;
	}

	public void setConchivive(Conchivive conchivive) {
		this.conchivive = conchivive;
	}

	@Inject
	private Conversation conversation;

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}
	
	private Integer pazienteId; 

	public Integer getPazienteId() {
		return pazienteId;
	}

	public void setPazienteId(Integer pazienteId) {
		this.pazienteId = pazienteId;
	}
	
	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
 

	/*
	 * Support listing and POSTing back Indirizzoresidenza entities (e.g. from
	 * inside an HtmlSelectOneMenu)
	 */

	public List<SelectItem> getAllItem() {

		
		
		CriteriaQuery<Conchivive> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Conchivive.class);
		List<Conchivive> list = this.entityManager.createQuery(
				criteria.select(criteria.from(Conchivive.class)))
				.getResultList();
		List<SelectItem> items = new ArrayList<SelectItem>(list.size());
		
		items.add(new SelectItem(null,"-- Seleziona un elemento --"));
		for (Conchivive c : list){
			items.add(new SelectItem(c.getConChiViveId(), c.getDescrizione()));
		}
		//	items.add(new SelectItem(null,"-- Seleziona un elemento --"));
		
		return items;
		
	}
	
	public List<Conchivive> getAll() {
		CriteriaQuery<Conchivive> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(Conchivive.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(Conchivive.class)))
				.getResultList();
	}
	

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Conchivive add = new Conchivive();

	public Conchivive getAdd() {
		return this.add;
	}

	public Conchivive getAdded() {
		Conchivive added = this.add;
		this.add = new Conchivive();
		return added;
	}
}