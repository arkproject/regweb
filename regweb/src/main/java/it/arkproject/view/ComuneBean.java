package it.arkproject.view;

import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Comune;
import it.arkproject.model.Indirizzoresidenza;
import it.arkproject.model.JsfSpringSecUsers;
import it.arkproject.model.Provinciaregione;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.Iterator;


/**
 * Backing bean for Comune entities.
 * <p>
 * This class provides CRUD functionality for all Comune entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ComuneBean implements Serializable
{

   private static final long serialVersionUID = 1L;

   /*
    * Support creating and retrieving Comune entities
    */

   private Integer id;

   public Integer getId()
   {
      return this.id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   private Comune comune;

   public Comune getComune()
   {
      return this.comune;
   }

   public void setComune(Comune comune)
   {
      this.comune = comune;
   }

   @Inject
   private Conversation conversation;

   @PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
   private EntityManager entityManager;

   public String create()
   {

      this.conversation.begin();
      this.conversation.setTimeout(1800000L);
      return "create?faces-redirect=true";
   }

   public void retrieve()
   {

      if (FacesContext.getCurrentInstance().isPostback())
      {
         return;
      }

      if (this.conversation.isTransient())
      {
         this.conversation.begin();
         this.conversation.setTimeout(1800000L);
      }

      if (this.id == null)
      {
         this.comune = this.example;
      }
      else
      {
         this.comune = findById(getId());
      }
   }

   public Comune findById(Integer id)
   {

      return this.entityManager.find(Comune.class, id);
   }

   /*
    * Support updating and deleting Comune entities
    */

   public String update()
   {
      this.conversation.end();

      try
      {
         if (this.id == null)
         {
            this.entityManager.persist(this.comune);
            return "search?faces-redirect=true";
         }
         else
         {
            this.entityManager.merge(this.comune);
            return "view?faces-redirect=true&id=" + this.comune.getComuneId();
         }
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   public String delete()
   {
      this.conversation.end();

      try
      {
         Comune deletableEntity = findById(getId());
         Provinciaregione provinciaregione = deletableEntity.getProvinciaregione();
         provinciaregione.getComunes().remove(deletableEntity);
         deletableEntity.setProvinciaregione(null);
         this.entityManager.merge(provinciaregione);
         Iterator<Indirizzoresidenza> iterIndirizzoresidenzas = deletableEntity.getIndirizzoresidenzas().iterator();
         for (; iterIndirizzoresidenzas.hasNext();)
         {
            Indirizzoresidenza nextInIndirizzoresidenzas = iterIndirizzoresidenzas.next();
            nextInIndirizzoresidenzas.setComune(null);
            iterIndirizzoresidenzas.remove();
            this.entityManager.merge(nextInIndirizzoresidenzas);
         }
         Iterator<JsfSpringSecUsers> iterJsfSpringSecUserses = deletableEntity.getJsfSpringSecUserses().iterator();
         for (; iterJsfSpringSecUserses.hasNext();)
         {
            JsfSpringSecUsers nextInJsfSpringSecUserses = iterJsfSpringSecUserses.next();
            nextInJsfSpringSecUserses.setComune(null);
            iterJsfSpringSecUserses.remove();
            this.entityManager.merge(nextInJsfSpringSecUserses);
         }
         Iterator<Anagraficapaziente> iterAnagraficapazientes = deletableEntity.getAnagraficapazientes().iterator();
         for (; iterAnagraficapazientes.hasNext();)
         {
            Anagraficapaziente nextInAnagraficapazientes = iterAnagraficapazientes.next();
            nextInAnagraficapazientes.setComune(null);
            iterAnagraficapazientes.remove();
            this.entityManager.merge(nextInAnagraficapazientes);
         }
         this.entityManager.remove(deletableEntity);
         this.entityManager.flush();
         return "search?faces-redirect=true";
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   /*
    * Support searching Comune entities with pagination
    */

   private int page;
   private long count;
   private List<Comune> pageItems;

   private Comune example = new Comune();

   public int getPage()
   {
      return this.page;
   }

   public void setPage(int page)
   {
      this.page = page;
   }

   public int getPageSize()
   {
      return 10;
   }

   public Comune getExample()
   {
      return this.example;
   }

   public void setExample(Comune example)
   {
      this.example = example;
   }

   public void search()
   {
      this.page = 0;
   }

   public void paginate()
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

      // Populate this.count

      CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
      Root<Comune> root = countCriteria.from(Comune.class);
      countCriteria = countCriteria.select(builder.count(root)).where(
            getSearchPredicates(root));
      this.count = this.entityManager.createQuery(countCriteria)
            .getSingleResult();

      // Populate this.pageItems

      CriteriaQuery<Comune> criteria = builder.createQuery(Comune.class);
      root = criteria.from(Comune.class);
      TypedQuery<Comune> query = this.entityManager.createQuery(criteria
            .select(root).where(getSearchPredicates(root)));
      query.setFirstResult(this.page * getPageSize()).setMaxResults(
            getPageSize());
      this.pageItems = query.getResultList();
   }

   private Predicate[] getSearchPredicates(Root<Comune> root)
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
      List<Predicate> predicatesList = new ArrayList<Predicate>();

      Provinciaregione provinciaregione = this.example.getProvinciaregione();
      if (provinciaregione != null)
      {
         predicatesList.add(builder.equal(root.get("provinciaregione"), provinciaregione));
      }
      String denominazione = this.example.getDenominazione();
      if (denominazione != null && !"".equals(denominazione))
      {
         predicatesList.add(builder.like(builder.lower(root.<String> get("denominazione")), '%' + denominazione.toLowerCase() + '%'));
      }
      int codiceRegione = this.example.getCodiceRegione();
      if (codiceRegione != 0)
      {
         predicatesList.add(builder.equal(root.get("codiceRegione"), codiceRegione));
      }
      String codiceCatastale = this.example.getCodiceCatastale();
      if (codiceCatastale != null && !"".equals(codiceCatastale))
      {
         predicatesList.add(builder.like(builder.lower(root.<String> get("codiceCatastale")), '%' + codiceCatastale.toLowerCase() + '%'));
      }
      String ripartizioneGeografica = this.example.getRipartizioneGeografica();
      if (ripartizioneGeografica != null && !"".equals(ripartizioneGeografica))
      {
         predicatesList.add(builder.like(builder.lower(root.<String> get("ripartizioneGeografica")), '%' + ripartizioneGeografica.toLowerCase() + '%'));
      }

      return predicatesList.toArray(new Predicate[predicatesList.size()]);
   }

   public List<Comune> getPageItems()
   {
      return this.pageItems;
   }

   public long getCount()
   {
      return this.count;
   }

   /*
    * Support listing and POSTing back Comune entities (e.g. from inside an
    * HtmlSelectOneMenu)
    */

   public List<Comune> getAll()
   {

      CriteriaQuery<Comune> criteria = this.entityManager
            .getCriteriaBuilder().createQuery(Comune.class);
      return this.entityManager.createQuery(
            criteria.select(criteria.from(Comune.class))).getResultList();
   }

   @Resource
   private SessionContext sessionContext;

   public Converter getConverter()
   {

      final ComuneBean ejbProxy = this.sessionContext.getBusinessObject(ComuneBean.class);

      return new Converter()
      {

         @Override
         public Object getAsObject(FacesContext context,
               UIComponent component, String value)
         {

            return ejbProxy.findById(Integer.valueOf(value));
         }

         @Override
         public String getAsString(FacesContext context,
               UIComponent component, Object value)
         {

            if (value == null)
            {
               return "";
            }

            return String.valueOf(((Comune) value).getComuneId());
         }
      };
   }

   /*
    * Support adding children to bidirectional, one-to-many tables
    */

   private Comune add = new Comune();

   public Comune getAdd()
   {
      return this.add;
   }

   public Comune getAdded()
   {
      Comune added = this.add;
      this.add = new Comune();
      return added;
   }
}