package it.arkproject.model;

// Generated 5-nov-2014 12.09.28 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Livelloistruzione generated by hbm2java
 */
@Entity
@Table(name = "livelloistruzione"
      , catalog = "regweb")
public class Livelloistruzione implements java.io.Serializable
{

   private Integer livelloIstruzioneId;
   private String descrizione;
   private Set<Demografica> demograficas = new HashSet<Demografica>(0);

   public Livelloistruzione()
   {
   }

   public Livelloistruzione(String descrizione, Set<Demografica> demograficas)
   {
      this.descrizione = descrizione;
      this.demograficas = demograficas;
   }

   @Id
   @GeneratedValue(strategy = IDENTITY)
   @Column(name = "livelloIstruzioneID", unique = true, nullable = false)
   public Integer getLivelloIstruzioneId()
   {
      return this.livelloIstruzioneId;
   }

   public void setLivelloIstruzioneId(Integer livelloIstruzioneId)
   {
      this.livelloIstruzioneId = livelloIstruzioneId;
   }

   @Column(name = "Descrizione", length = 45)
   public String getDescrizione()
   {
      return this.descrizione;
   }

   public void setDescrizione(String descrizione)
   {
      this.descrizione = descrizione;
   }

   @OneToMany(fetch = FetchType.LAZY, mappedBy = "livelloistruzione")
   public Set<Demografica> getDemograficas()
   {
      return this.demograficas;
   }

   public void setDemograficas(Set<Demografica> demograficas)
   {
      this.demograficas = demograficas;
   }
   
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Livelloistruzione) && (livelloIstruzioneId != null) ? livelloIstruzioneId
				.equals(((Livelloistruzione) obj).livelloIstruzioneId) : (obj == this);
	}

	@Override
	public int hashCode() {
		return (livelloIstruzioneId != null) ? (this.getClass().hashCode() + livelloIstruzioneId
				.hashCode()) : super.hashCode();
	}

	@Override
	public String toString() {
		return String.format("Livelloistruzione[%d, %s]", livelloIstruzioneId, descrizione);
	}

}
