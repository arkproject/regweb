package it.arkproject.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EventiPazienteSubTable implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nomeScheda;

    private List<Eventopaziente> eventoPazientes;

    public EventiPazienteSubTable() {
        eventoPazientes = new ArrayList<>();
    }

    public EventiPazienteSubTable(String nomeScheda) {
        this.nomeScheda = nomeScheda;
        eventoPazientes = new ArrayList<>();
    }

    public String getNomeScheda() {
        return nomeScheda;
    }

    public void setNomeScheda(String nomeScheda) {
        this.nomeScheda = nomeScheda;
    }

    public List<Eventopaziente> getEventoPazientes() {
        return eventoPazientes;
    }

    public void setEventoPazientes(List<Eventopaziente> eventoPazientes) {
        this.eventoPazientes = eventoPazientes;
    }
}
