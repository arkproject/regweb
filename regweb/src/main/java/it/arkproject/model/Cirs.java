package it.arkproject.model;
// Generated 26-nov-2014 21.57.19 by Hibernate Tools 4.3.1


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Cirs generated by hbm2java
 */
@Entity
@Table(name="cirs"
    ,catalog="regweb"
)
public class Cirs  implements java.io.Serializable {


     private Integer cirsId;
     private Anagraficapaziente anagraficapaziente;
     private int cardiologico;
     private int ipertensione;
     private int vascolare;
     private int respiratorio;
     private int occhioOrecchioNasoGola;
     private int gastroenterico;
     private int intestino;
     private int epatico;
     private int renale;
     private int genitoUrinario;
     private int muscoloScheletrico;
     private int neurologico;
     private int endocrinicoMetabolico;
     private int cognitivoPsichiatricoComportamentale;
     private Date dataCompilazione;

    public Cirs() {
    }

    public Cirs(Anagraficapaziente anagraficapaziente, int cardiologico, int ipertensione, int vascolare, int respiratorio, int occhioOrecchioNasoGola, int gastroenterico, int intestino, int epatico, int renale, int genitoUrinario, int muscoloScheletrico, int neurologico, int endocrinicoMetabolico, int cognitivoPsichiatricoComportamentale,Date dataCompilazione) {
       this.anagraficapaziente = anagraficapaziente;
       this.cardiologico = cardiologico;
       this.ipertensione = ipertensione;
       this.vascolare = vascolare;
       this.respiratorio = respiratorio;
       this.occhioOrecchioNasoGola = occhioOrecchioNasoGola;
       this.gastroenterico = gastroenterico;
       this.intestino = intestino;
       this.epatico = epatico;
       this.renale = renale;
       this.genitoUrinario = genitoUrinario;
       this.muscoloScheletrico = muscoloScheletrico;
       this.neurologico = neurologico;
       this.endocrinicoMetabolico = endocrinicoMetabolico;
       this.cognitivoPsichiatricoComportamentale = cognitivoPsichiatricoComportamentale;
       this.dataCompilazione = dataCompilazione;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="cirsID", unique=true, nullable=false)
    public Integer getCirsId() {
        return this.cirsId;
    }
    
    public void setCirsId(Integer cirsId) {
        this.cirsId = cirsId;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="anagraficapaziente_pazienteID", nullable=false)
    public Anagraficapaziente getAnagraficapaziente() {
        return this.anagraficapaziente;
    }
    
    public void setAnagraficapaziente(Anagraficapaziente anagraficapaziente) {
        this.anagraficapaziente = anagraficapaziente;
    }

    
    @Column(name="cardiologico", nullable=false)
    public int getCardiologico() {
        return this.cardiologico;
    }
    
    public void setCardiologico(int cardiologico) {
        this.cardiologico = cardiologico;
    }

    
    @Column(name="ipertensione", nullable=false)
    public int getIpertensione() {
        return this.ipertensione;
    }
    
    public void setIpertensione(int ipertensione) {
        this.ipertensione = ipertensione;
    }

    
    @Column(name="vascolare", nullable=false)
    public int getVascolare() {
        return this.vascolare;
    }
    
    public void setVascolare(int vascolare) {
        this.vascolare = vascolare;
    }

    
    @Column(name="respiratorio", nullable=false)
    public int getRespiratorio() {
        return this.respiratorio;
    }
    
    public void setRespiratorio(int respiratorio) {
        this.respiratorio = respiratorio;
    }

    
    @Column(name="occhioOrecchioNasoGola", nullable=false)
    public int getOcchioOrecchioNasoGola() {
        return this.occhioOrecchioNasoGola;
    }
    
    public void setOcchioOrecchioNasoGola(int occhioOrecchioNasoGola) {
        this.occhioOrecchioNasoGola = occhioOrecchioNasoGola;
    }

    
    @Column(name="gastroenterico", nullable=false)
    public int getGastroenterico() {
        return this.gastroenterico;
    }
    
    public void setGastroenterico(int gastroenterico) {
        this.gastroenterico = gastroenterico;
    }

    
    @Column(name="intestino", nullable=false)
    public int getIntestino() {
        return this.intestino;
    }
    
    public void setIntestino(int intestino) {
        this.intestino = intestino;
    }

    
    @Column(name="epatico", nullable=false)
    public int getEpatico() {
        return this.epatico;
    }
    
    public void setEpatico(int epatico) {
        this.epatico = epatico;
    }

    
    @Column(name="renale", nullable=false)
    public int getRenale() {
        return this.renale;
    }
    
    public void setRenale(int renale) {
        this.renale = renale;
    }

    
    @Column(name="genitoUrinario", nullable=false)
    public int getGenitoUrinario() {
        return this.genitoUrinario;
    }
    
    public void setGenitoUrinario(int genitoUrinario) {
        this.genitoUrinario = genitoUrinario;
    }

    
    @Column(name="muscoloScheletrico", nullable=false)
    public int getMuscoloScheletrico() {
        return this.muscoloScheletrico;
    }
    
    public void setMuscoloScheletrico(int muscoloScheletrico) {
        this.muscoloScheletrico = muscoloScheletrico;
    }

    
    @Column(name="neurologico", nullable=false)
    public int getNeurologico() {
        return this.neurologico;
    }
    
    public void setNeurologico(int neurologico) {
        this.neurologico = neurologico;
    }

    
    @Column(name="endocrinicoMetabolico", nullable=false)
    public int getEndocrinicoMetabolico() {
        return this.endocrinicoMetabolico;
    }
    
    public void setEndocrinicoMetabolico(int endocrinicoMetabolico) {
        this.endocrinicoMetabolico = endocrinicoMetabolico;
    }

    
    @Column(name="cognitivoPsichiatricoComportamentale", nullable=false)
    public int getCognitivoPsichiatricoComportamentale() {
        return this.cognitivoPsichiatricoComportamentale;
    }
    
    public void setCognitivoPsichiatricoComportamentale(int cognitivoPsichiatricoComportamentale) {
        this.cognitivoPsichiatricoComportamentale = cognitivoPsichiatricoComportamentale;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="dataCompilazione", nullable=false, length=10)
    public Date getDataCompilazione() {
        return this.dataCompilazione;
    }
    
    public void setDataCompilazione(Date dataCompilazione) {
        this.dataCompilazione = dataCompilazione;
    }

}


