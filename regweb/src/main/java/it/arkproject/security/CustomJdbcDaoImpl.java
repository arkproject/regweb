package it.arkproject.security;

//
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
   
/**
 *
 * @author ArK
 */
public class CustomJdbcDaoImpl extends JdbcDaoImpl {

    @Override
    protected UserDetails createUserDetails(String username,
            UserDetails userFromUserQuery, List<GrantedAuthority> combinedAuthorities) {
        String returnUsername = userFromUserQuery.getUsername();
        if (!isUsernameBasedPrimaryKey()) {
            returnUsername = username;
        }
 
        return new DettagliUser(returnUsername, userFromUserQuery.getPassword(),
                userFromUserQuery.isEnabled(), true, true, true,
                combinedAuthorities, 
                ((DettagliUser) userFromUserQuery).getNomeCognome(),
        ((DettagliUser) userFromUserQuery).getEmail(),
                ((DettagliUser) userFromUserQuery).getIdIstituto(),
                ((DettagliUser) userFromUserQuery).getIdResidenza(),
                ((DettagliUser) userFromUserQuery).getIndirizzo(),
                ((DettagliUser) userFromUserQuery).getTelefono()
        );
    }

    @Override
    protected List<UserDetails> loadUsersByUsername(String username) {
        return getJdbcTemplate().query(getUsersByUsernameQuery(),
                new String[]{username},
                new RowMapper<UserDetails>() {
                    public UserDetails mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                        String username = rs.getString(1);
                        String password = rs.getString(2);
                        boolean enabled = rs.getBoolean(3);
                        String nomeCognome = rs.getString(4);
                        String email = rs.getString(5);
                        String idIstituto = rs.getString(6);
                        String idResidenza = rs.getString(7);
                        String indirizzo = rs.getString(8);
                        String telefono = rs.getString(9);
                        return new DettagliUser(username, password,
                                enabled, true, true, true,
                                AuthorityUtils.NO_AUTHORITIES,
                                nomeCognome,email,idIstituto,idResidenza,
                                indirizzo,telefono);
                    }
                });
    }

}

//   public CustomJdbcDaoImpl(){
//        super();
//    }
//    @Override
//    protected UserDetails createUserDetails(String username, UserDetails userFromUserQuery, List<GrantedAuthority> combinedAuthorities) {
//        return super.createUserDetails(username, userFromUserQuery, combinedAuthorities); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    protected JdbcTemplate createJdbcTemplate(DataSource dataSource) {
//        return super.createJdbcTemplate(dataSource); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return super.loadUserByUsername(username); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    protected List<UserDetails> loadUsersByUsername(String username) {
//        return super.loadUsersByUsername(username); //To change body of generated methods, choose Tools | Templates.
//    }
//    @Override
//    protected UserDetails createUserDetails(String username, UserDetails userFromUserQuery, List<GrantedAuthority> combinedAuthorities) {
//        String returnUsername = userFromUserQuery.getUsername();
//        if (!isUsernameBasedPrimaryKey()) {
//            returnUsername = username;
//        }
//
//        return new DettagliUser(returnUsername, userFromUserQuery.getPassword(), userFromUserQuery.isEnabled(), true, true, true, combinedAuthorities, ((DettagliUser) userFromUserQuery).getNome(), ((DettagliUser) userFromUserQuery).getCognome());
//    }
//
//
//    @Override
//    protected List<UserDetails> loadUsersByUsername(String username) {
//        return getJdbcTemplate().query(getUsersByUsernameQuery(),
//                new String[]{username},
//                new RowMapper<UserDetails>() {
//                    public UserDetails mapRow(ResultSet rs, int rowNum)
//                            throws SQLException {
//                        String username = rs.getString(1);
//                        String password = rs.getString(2);
//                        boolean enabled = rs.getBoolean(3);
//                        String nome = rs.getString(4);
//                        String cognome = rs.getString(5);
//                        return new DettagliUser(username, password,
//                                enabled, true, true, true,
//                                AuthorityUtils.NO_AUTHORITIES, nome, cognome);
//                    }
//                });
//    }

