/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.arkproject.security;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

//@ManagedBean(name = "securityBean")
//@SessionScoped

@Named(value = "securityBean")
@SessionScoped
public class SecurityHolderBean implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isUserAdmin = false;
    private boolean isUser = false;
    private boolean isUserGuest = false;

    public boolean getIsUserAdmin() {
        isUserAdmin = false;
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();
        for (GrantedAuthority grantedAuthority : authentication
                .getAuthorities()) {
            if ("ROLE_ADMIN".equals(grantedAuthority.getAuthority())) {
                isUserAdmin = true;
                break;
            }
        }
        return isUserAdmin;
    }

    public boolean getIsUser() {
        isUser = false;
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();
        for (GrantedAuthority grantedAuthority : authentication
                .getAuthorities()) {
            if("ROLE_USER".equals(grantedAuthority.getAuthority())) {
                isUser = true;
                break;
            }
        }
        return isUser;
    }

    public boolean getIsUserGuest() {
        isUserGuest = false;
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();
        for (GrantedAuthority grantedAuthority : authentication
                .getAuthorities()) {
            if ("ROLE_GUEST".equals(grantedAuthority.getAuthority())) {
                isUserGuest = true;
                break;
            }
        }
        return isUserGuest;
    }

    public void setUserAdmin(boolean isUserAdmin) {
        this.isUserAdmin = isUserAdmin;
    }

    public void setIsUser(boolean isUser) {
        this.isUser = isUser;
    }
    
    public void setUserGuest(boolean isUserGuest) {
        this.isUserGuest = isUserGuest;
    }
}
