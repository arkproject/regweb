package it.arkproject.security;
 
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class DettagliUser extends User {

    private static final long serialVersionUID = 1L;
    
    private String nomeCognome;
    private String email;
    private String idIstituto;
    private String idResidenza;
    private String indirizzo;
    private String telefono;

    public DettagliUser(String username, String password, boolean enabled,
            boolean accountNonExpired, boolean credentialsNonExpired,
            boolean accountNonLocked,
            Collection<? extends GrantedAuthority> authorities,
            String nomeCognome,String email,String idIstituto,
            String idResidenza,String indirizzo,String telefono
            ) {
        super(username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities);
        this.nomeCognome=nomeCognome;
        this.email = email;
        this.idIstituto = idIstituto;
        this.idResidenza = idResidenza;
        this.indirizzo = indirizzo;
        this.telefono = telefono;
    }


    public String getNomeCognome() {
        return nomeCognome;
    }

    public void setNomeCognome(String nomeCognome) {
        this.nomeCognome = nomeCognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdIstituto() {
        return idIstituto;
    }

    public void setIdIstituto(String idIstituto) {
        this.idIstituto = idIstituto;
    }

    public String getIdResidenza() {
        return idResidenza;
    }

    public void setIdResidenza(String idResidenza) {
        this.idResidenza = idResidenza;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    

}
