/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.arkproject.security;

import it.arkproject.model.Istituto;
import it.arkproject.view.IstitutoBean;

import java.io.Serializable;

import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

//@ManagedBean(name = "loginBean")
//@SessionScoped
@Named
@SessionScoped
public class LoginBean implements Serializable {

	// private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userName = null;
	private String password = null;
	private String passwordCheck;
	private String nomeCognome;
	private Integer idIstituto;
	
	// @ManagedProperty(value = "#{authenticationManager}")
	// AuthenticationManager authenticationManager = null;

	AuthenticationManager authenticationManager = null;

	/**
	 * Login.
	 *
	 * @return the string
	 */

	public String login() {
		try {
			Authentication request = new UsernamePasswordAuthenticationToken(
					this.getUserName(), this.getPassword());
			Authentication result = authenticationManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);

			/**
			 * Messaggio di benvenuto
			 */
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Successful",
							"Benvenuto nell'area protetta"));

			DettagliUser user = (DettagliUser) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();
			nomeCognome = user.getNomeCognome();
			if (user.getIdIstituto() != null) {
				idIstituto = Integer.parseInt(user.getIdIstituto());
			} else {
				idIstituto = null;
			}

			return "/pages/secure/dashboard/dashboard?faces-redirect=true&includeViewParams=true";
		} catch (AuthenticationException e) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Errore Accesso", e.getMessage()));
			return null;
		}
	}

	public String cancel() {
		return null;
	}

	public String logout() {

		SecurityContextHolder.clearContext();

		FacesContext ctx = FacesContext.getCurrentInstance();
		ExternalContext etx = ctx.getExternalContext();
		HttpSession session = (HttpSession) etx.getSession(false);
		session.invalidate();

		return "/pages/common/login?faces-redirect=true&includeViewParams=true";
	}

	public String unlock() {
		if (password.equals(this.passwordCheck)) {
			return "/pages/secure/dashboard/dashboard?faces-redirect=true";
		}
		return null;
	}

	
	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(
			AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNomeCognome() {
		return nomeCognome;
	}

	public void setNomeCognome(String nomeCognome) {
		this.nomeCognome = nomeCognome;
	}

	public Integer getIdIstituto() {
		return idIstituto;
	}

	public void setIdIstituto(Integer idIstituto) {
		this.idIstituto = idIstituto;
	}

	public String getPasswordCheck() {
		return passwordCheck;
	}

	public void setPasswordCheck(String passwordCheck) {
		this.passwordCheck = passwordCheck;
	}

}
