package it.arkproject.security.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.JsfSpringSecGroupAuthorities;
import it.arkproject.model.JsfSpringSecGroups;

/**
 * Backing bean for JsfSpringSecGroupAuthorities entities.
 * <p>
 * This class provides CRUD functionality for all JsfSpringSecGroupAuthorities entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class JsfSpringSecGroupAuthoritiesBean implements Serializable
{

   private static final long serialVersionUID = 1L;

   /*
    * Support creating and retrieving JsfSpringSecGroupAuthorities entities
    */

   private Integer id;

   public Integer getId()
   {
      return this.id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   private JsfSpringSecGroupAuthorities jsfSpringSecGroupAuthorities;

   public JsfSpringSecGroupAuthorities getJsfSpringSecGroupAuthorities()
   {
      return this.jsfSpringSecGroupAuthorities;
   }

   public void setJsfSpringSecGroupAuthorities(JsfSpringSecGroupAuthorities jsfSpringSecGroupAuthorities)
   {
      this.jsfSpringSecGroupAuthorities = jsfSpringSecGroupAuthorities;
   }

   @Inject
   private Conversation conversation;

   @PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
   private EntityManager entityManager;

   public String create()
   {

      this.conversation.begin();
      this.conversation.setTimeout(1800000L);
      return "create?faces-redirect=true";
   }

   public void retrieve()
   {

      if (FacesContext.getCurrentInstance().isPostback())
      {
         return;
      }

      if (this.conversation.isTransient())
      {
         this.conversation.begin();
         this.conversation.setTimeout(1800000L);
      }

      if (this.id == null)
      {
         this.jsfSpringSecGroupAuthorities = this.example;
      }
      else
      {
         this.jsfSpringSecGroupAuthorities = findById(getId());
      }
   }

   public JsfSpringSecGroupAuthorities findById(Integer id)
   {

      return this.entityManager.find(JsfSpringSecGroupAuthorities.class, id);
   }

   /*
    * Support updating and deleting JsfSpringSecGroupAuthorities entities
    */

   public String update()
   {
      this.conversation.end();

      try
      {
         if (this.id == null)
         {
            this.entityManager.persist(this.jsfSpringSecGroupAuthorities);
            return "search?faces-redirect=true";
         }
         else
         {
            this.entityManager.merge(this.jsfSpringSecGroupAuthorities);
            return "view?faces-redirect=true&id=" + this.jsfSpringSecGroupAuthorities.getJsfSpringSecGroupAuthoritiesAuthorityId();
         }
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   public String delete()
   {
      this.conversation.end();

      try
      {
         JsfSpringSecGroupAuthorities deletableEntity = findById(getId());
         JsfSpringSecGroups jsfSpringSecGroups = deletableEntity.getJsfSpringSecGroups();
         jsfSpringSecGroups.getJsfSpringSecGroupAuthoritieses().remove(deletableEntity);
         deletableEntity.setJsfSpringSecGroups(null);
         this.entityManager.merge(jsfSpringSecGroups);
         this.entityManager.remove(deletableEntity);
         this.entityManager.flush();
         return "search?faces-redirect=true";
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   /*
    * Support searching JsfSpringSecGroupAuthorities entities with pagination
    */

   private int page;
   private long count;
   private List<JsfSpringSecGroupAuthorities> pageItems;

   private JsfSpringSecGroupAuthorities example = new JsfSpringSecGroupAuthorities();

   public int getPage()
   {
      return this.page;
   }

   public void setPage(int page)
   {
      this.page = page;
   }

   public int getPageSize()
   {
      return 10;
   }

   public JsfSpringSecGroupAuthorities getExample()
   {
      return this.example;
   }

   public void setExample(JsfSpringSecGroupAuthorities example)
   {
      this.example = example;
   }

   public void search()
   {
      this.page = 0;
   }

   public void paginate()
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

      // Populate this.count

      CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
      Root<JsfSpringSecGroupAuthorities> root = countCriteria.from(JsfSpringSecGroupAuthorities.class);
      countCriteria = countCriteria.select(builder.count(root)).where(
            getSearchPredicates(root));
      this.count = this.entityManager.createQuery(countCriteria)
            .getSingleResult();

      // Populate this.pageItems

      CriteriaQuery<JsfSpringSecGroupAuthorities> criteria = builder.createQuery(JsfSpringSecGroupAuthorities.class);
      root = criteria.from(JsfSpringSecGroupAuthorities.class);
      TypedQuery<JsfSpringSecGroupAuthorities> query = this.entityManager.createQuery(criteria
            .select(root).where(getSearchPredicates(root)));
      query.setFirstResult(this.page * getPageSize()).setMaxResults(
            getPageSize());
      this.pageItems = query.getResultList();
   }

   private Predicate[] getSearchPredicates(Root<JsfSpringSecGroupAuthorities> root)
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
      List<Predicate> predicatesList = new ArrayList<Predicate>();

      JsfSpringSecGroups jsfSpringSecGroups = this.example.getJsfSpringSecGroups();
      if (jsfSpringSecGroups != null)
      {
         predicatesList.add(builder.equal(root.get("jsfSpringSecGroups"), jsfSpringSecGroups));
      }
      String jsfSpringSecGroupAuthoritiesAuthority = this.example.getJsfSpringSecGroupAuthoritiesAuthority();
      if (jsfSpringSecGroupAuthoritiesAuthority != null && !"".equals(jsfSpringSecGroupAuthoritiesAuthority))
      {
         predicatesList.add(builder.like(builder.lower(root.<String> get("jsfSpringSecGroupAuthoritiesAuthority")), '%' + jsfSpringSecGroupAuthoritiesAuthority.toLowerCase() + '%'));
      }

      return predicatesList.toArray(new Predicate[predicatesList.size()]);
   }

   public List<JsfSpringSecGroupAuthorities> getPageItems()
   {
      return this.pageItems;
   }

   public long getCount()
   {
      return this.count;
   }

   /*
    * Support listing and POSTing back JsfSpringSecGroupAuthorities entities (e.g. from inside an
    * HtmlSelectOneMenu)
    */

   public List<JsfSpringSecGroupAuthorities> getAll()
   {

      CriteriaQuery<JsfSpringSecGroupAuthorities> criteria = this.entityManager
            .getCriteriaBuilder().createQuery(JsfSpringSecGroupAuthorities.class);
      return this.entityManager.createQuery(
            criteria.select(criteria.from(JsfSpringSecGroupAuthorities.class))).getResultList();
   }

   @Resource
   private SessionContext sessionContext;

   public Converter getConverter()
   {

      final JsfSpringSecGroupAuthoritiesBean ejbProxy = this.sessionContext.getBusinessObject(JsfSpringSecGroupAuthoritiesBean.class);

      return new Converter()
      {

         @Override
         public Object getAsObject(FacesContext context,
               UIComponent component, String value)
         {

            return ejbProxy.findById(Integer.valueOf(value));
         }

         @Override
         public String getAsString(FacesContext context,
               UIComponent component, Object value)
         {

            if (value == null)
            {
               return "";
            }

            return String.valueOf(((JsfSpringSecGroupAuthorities) value).getJsfSpringSecGroupAuthoritiesAuthorityId());
         }
      };
   }

   /*
    * Support adding children to bidirectional, one-to-many tables
    */

   private JsfSpringSecGroupAuthorities add = new JsfSpringSecGroupAuthorities();

   public JsfSpringSecGroupAuthorities getAdd()
   {
      return this.add;
   }

   public JsfSpringSecGroupAuthorities getAdded()
   {
      JsfSpringSecGroupAuthorities added = this.add;
      this.add = new JsfSpringSecGroupAuthorities();
      return added;
   }
}