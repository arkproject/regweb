package it.arkproject.security.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.JsfSpringSecGroups;
import it.arkproject.model.JsfSpringSecGroupAuthorities;
import it.arkproject.model.JsfSpringSecGroupMembers;
import java.util.Iterator;

/**
 * Backing bean for JsfSpringSecGroups entities.
 * <p>
 * This class provides CRUD functionality for all JsfSpringSecGroups entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class JsfSpringSecGroupsBean implements Serializable
{

   private static final long serialVersionUID = 1L;

   /*
    * Support creating and retrieving JsfSpringSecGroups entities
    */

   private Integer id;

   public Integer getId()
   {
      return this.id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   private JsfSpringSecGroups jsfSpringSecGroups;

   public JsfSpringSecGroups getJsfSpringSecGroups()
   {
      return this.jsfSpringSecGroups;
   }

   public void setJsfSpringSecGroups(JsfSpringSecGroups jsfSpringSecGroups)
   {
      this.jsfSpringSecGroups = jsfSpringSecGroups;
   }

   @Inject
   private Conversation conversation;

   @PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
   private EntityManager entityManager;

   public String create()
   {

      this.conversation.begin();
      this.conversation.setTimeout(1800000L);
      return "create?faces-redirect=true";
   }

   public void retrieve()
   {

      if (FacesContext.getCurrentInstance().isPostback())
      {
         return;
      }

      if (this.conversation.isTransient())
      {
         this.conversation.begin();
         this.conversation.setTimeout(1800000L);
      }

      if (this.id == null)
      {
         this.jsfSpringSecGroups = this.example;
      }
      else
      {
         this.jsfSpringSecGroups = findById(getId());
      }
   }

   public JsfSpringSecGroups findById(Integer id)
   {

      return this.entityManager.find(JsfSpringSecGroups.class, id);
   }

   /*
    * Support updating and deleting JsfSpringSecGroups entities
    */

   public String update()
   {
      this.conversation.end();

      try
      {
         if (this.id == null)
         {
            this.entityManager.persist(this.jsfSpringSecGroups);
            return "search?faces-redirect=true";
         }
         else
         {
            this.entityManager.merge(this.jsfSpringSecGroups);
            return "view?faces-redirect=true&id=" + this.jsfSpringSecGroups.getJsfSpringSecGroupsGroupId();
         }
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   public String delete()
   {
      this.conversation.end();

      try
      {
         JsfSpringSecGroups deletableEntity = findById(getId());
         Iterator<JsfSpringSecGroupAuthorities> iterJsfSpringSecGroupAuthoritieses = deletableEntity.getJsfSpringSecGroupAuthoritieses().iterator();
         for (; iterJsfSpringSecGroupAuthoritieses.hasNext();)
         {
            JsfSpringSecGroupAuthorities nextInJsfSpringSecGroupAuthoritieses = iterJsfSpringSecGroupAuthoritieses.next();
            nextInJsfSpringSecGroupAuthoritieses.setJsfSpringSecGroups(null);
            iterJsfSpringSecGroupAuthoritieses.remove();
            this.entityManager.merge(nextInJsfSpringSecGroupAuthoritieses);
         }
         Iterator<JsfSpringSecGroupMembers> iterJsfSpringSecGroupMemberses = deletableEntity.getJsfSpringSecGroupMemberses().iterator();
         for (; iterJsfSpringSecGroupMemberses.hasNext();)
         {
            JsfSpringSecGroupMembers nextInJsfSpringSecGroupMemberses = iterJsfSpringSecGroupMemberses.next();
            nextInJsfSpringSecGroupMemberses.setJsfSpringSecGroups(null);
            iterJsfSpringSecGroupMemberses.remove();
            this.entityManager.merge(nextInJsfSpringSecGroupMemberses);
         }
         this.entityManager.remove(deletableEntity);
         this.entityManager.flush();
         return "search?faces-redirect=true";
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   /*
    * Support searching JsfSpringSecGroups entities with pagination
    */

   private int page;
   private long count;
   private List<JsfSpringSecGroups> pageItems;

   private JsfSpringSecGroups example = new JsfSpringSecGroups();

   public int getPage()
   {
      return this.page;
   }

   public void setPage(int page)
   {
      this.page = page;
   }

   public int getPageSize()
   {
      return 10;
   }

   public JsfSpringSecGroups getExample()
   {
      return this.example;
   }

   public void setExample(JsfSpringSecGroups example)
   {
      this.example = example;
   }

   public void search()
   {
      this.page = 0;
   }

   public void paginate()
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

      // Populate this.count

      CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
      Root<JsfSpringSecGroups> root = countCriteria.from(JsfSpringSecGroups.class);
      countCriteria = countCriteria.select(builder.count(root)).where(
            getSearchPredicates(root));
      this.count = this.entityManager.createQuery(countCriteria)
            .getSingleResult();

      // Populate this.pageItems

      CriteriaQuery<JsfSpringSecGroups> criteria = builder.createQuery(JsfSpringSecGroups.class);
      root = criteria.from(JsfSpringSecGroups.class);
      TypedQuery<JsfSpringSecGroups> query = this.entityManager.createQuery(criteria
            .select(root).where(getSearchPredicates(root)));
      query.setFirstResult(this.page * getPageSize()).setMaxResults(
            getPageSize());
      this.pageItems = query.getResultList();
   }

   private Predicate[] getSearchPredicates(Root<JsfSpringSecGroups> root)
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
      List<Predicate> predicatesList = new ArrayList<Predicate>();

      String jsfSpringSecGroupsGroupName = this.example.getJsfSpringSecGroupsGroupName();
      if (jsfSpringSecGroupsGroupName != null && !"".equals(jsfSpringSecGroupsGroupName))
      {
         predicatesList.add(builder.like(builder.lower(root.<String> get("jsfSpringSecGroupsGroupName")), '%' + jsfSpringSecGroupsGroupName.toLowerCase() + '%'));
      }

      return predicatesList.toArray(new Predicate[predicatesList.size()]);
   }

   public List<JsfSpringSecGroups> getPageItems()
   {
      return this.pageItems;
   }

   public long getCount()
   {
      return this.count;
   }

   /*
    * Support listing and POSTing back JsfSpringSecGroups entities (e.g. from inside an
    * HtmlSelectOneMenu)
    */

   public List<JsfSpringSecGroups> getAll()
   {

      CriteriaQuery<JsfSpringSecGroups> criteria = this.entityManager
            .getCriteriaBuilder().createQuery(JsfSpringSecGroups.class);
      return this.entityManager.createQuery(
            criteria.select(criteria.from(JsfSpringSecGroups.class))).getResultList();
   }

   @Resource
   private SessionContext sessionContext;

   public Converter getConverter()
   {

      final JsfSpringSecGroupsBean ejbProxy = this.sessionContext.getBusinessObject(JsfSpringSecGroupsBean.class);

      return new Converter()
      {

         @Override
         public Object getAsObject(FacesContext context,
               UIComponent component, String value)
         {

            return ejbProxy.findById(Integer.valueOf(value));
         }

         @Override
         public String getAsString(FacesContext context,
               UIComponent component, Object value)
         {

            if (value == null)
            {
               return "";
            }

            return String.valueOf(((JsfSpringSecGroups) value).getJsfSpringSecGroupsGroupId());
         }
      };
   }

   /*
    * Support adding children to bidirectional, one-to-many tables
    */

   private JsfSpringSecGroups add = new JsfSpringSecGroups();

   public JsfSpringSecGroups getAdd()
   {
      return this.add;
   }

   public JsfSpringSecGroups getAdded()
   {
      JsfSpringSecGroups added = this.add;
      this.add = new JsfSpringSecGroups();
      return added;
   }
}