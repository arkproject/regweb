package it.arkproject.security.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.Anagraficapaziente;
import it.arkproject.model.Indirizzoresidenza;
import it.arkproject.model.JsfSpringSecUsers;
import it.arkproject.model.Comune;
import it.arkproject.model.Istituto;
import it.arkproject.model.JsfSpringSecGroupMembers;
import it.arkproject.model.JsfSpringSecRoles;
import it.arkproject.util.JsfUtil;

import java.util.Iterator;

/**
 * Backing bean for JsfSpringSecUsers entities.
 * <p>
 * This class provides CRUD functionality for all JsfSpringSecUsers entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class JsfSpringSecUsersBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving JsfSpringSecUsers entities
	 */

	private Integer id;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	private JsfSpringSecUsers jsfSpringSecUsers;

	public JsfSpringSecUsers getJsfSpringSecUsers() {
		return jsfSpringSecUsers;
	}

	public void setJsfSpringSecUsers(JsfSpringSecUsers jsfSpringSecUsers) {
		this.jsfSpringSecUsers = jsfSpringSecUsers;
	}

	@Inject
	private Conversation conversation;

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	private Integer pazienteId;

	public Integer getPazienteId() {
		return pazienteId;
	}

	public void setPazienteId(Integer pazienteId) {
		this.pazienteId = pazienteId;
	}

	@PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		if (facesContext.getExternalContext().getRequestParameterMap()
				.get("pazienteId") != null) {
			pazienteId = Integer.parseInt(facesContext.getExternalContext()
					.getRequestParameterMap().get("pazienteId"));
		}

		// this.conversation.begin();
		// this.conversation.setTimeout(1800000L);
		this.username = null;
		jsfSpringSecUsers = new JsfSpringSecUsers();
		return "/pages/secure/dashboard/user/userprofile?faces-redirect=true";

	}

	public void retrieve() {

		// Integer id = null;
		//
		// FacesContext facesContext = FacesContext.getCurrentInstance();
		// if(facesContext.getExternalContext().getRequestParameterMap().get("id")
		// != null){
		// id = Integer.parseInt(facesContext.getExternalContext()
		// .getRequestParameterMap().get("id"));
		// }

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (username != null) {
			this.jsfSpringSecUsers = findById(getUsername());
		}

	}

	public JsfSpringSecUsers findById(Integer id) {
		return this.entityManager.find(JsfSpringSecUsers.class, id);
	}

	public JsfSpringSecUsers findById(String username) {

		CriteriaBuilder criteriaBuilder = this.entityManager
				.getCriteriaBuilder();
		CriteriaQuery<JsfSpringSecUsers> criteriaQuery = criteriaBuilder
				.createQuery(JsfSpringSecUsers.class);
		Root<JsfSpringSecUsers> root = criteriaQuery
				.from(JsfSpringSecUsers.class);
		criteriaQuery.select(root);
		criteriaQuery.where(criteriaBuilder.equal(root.get("jsfSpringSecUsersUsername"),
				username));

		TypedQuery<JsfSpringSecUsers> typedQuery =this.entityManager.createQuery(criteriaQuery);
		
		return typedQuery.getSingleResult();
	}

	/*
	 * Support updating and deleting Indirizzoresidenza entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.jsfSpringSecUsers);
				JsfUtil.addSuccessMessageDettagli(JsfUtil.EntitaNomeScheda.User
						.getText(),
						ResourceBundle.getBundle("propertiesFile/Bundle")
								.getString("UserUpdated"));
				return "/pages/secure/dashboard/dashboard";
			} else {
				this.entityManager.merge(this.jsfSpringSecUsers);
				JsfUtil.addSuccessMessageDettagli(JsfUtil.EntitaNomeScheda.User
						.getText(),
						ResourceBundle.getBundle("propertiesFile/Bundle")
								.getString("UserUpdated"));
				// return null;
				return "/pages/secure/dashboard/dashboard";
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		// this.conversation.end();
		try {
			return null;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support listing and POSTing back Indirizzoresidenza entities (e.g. from
	 * inside an HtmlSelectOneMenu)
	 */

	public List<JsfSpringSecUsers> getAll() {

		CriteriaQuery<JsfSpringSecUsers> criteria = this.entityManager
				.getCriteriaBuilder().createQuery(JsfSpringSecUsers.class);
		return this.entityManager.createQuery(
				criteria.select(criteria.from(JsfSpringSecUsers.class)))
				.getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private JsfSpringSecUsers add = new JsfSpringSecUsers();

	public JsfSpringSecUsers getAdd() {
		return this.add;
	}

	public JsfSpringSecUsers getAdded() {
		JsfSpringSecUsers added = this.add;
		this.add = new JsfSpringSecUsers();
		return added;
	}
	
	public void validatePassword(ComponentSystemEvent event) {
		 
		  FacesContext fc = FacesContext.getCurrentInstance();
	 
		  UIComponent components = event.getComponent();
	 
		  // get password
		  UIInput uiInputPassword = (UIInput) components.findComponent("password");
		  String password = uiInputPassword.getLocalValue() == null ? ""
			: uiInputPassword.getLocalValue().toString();
		  String passwordId = uiInputPassword.getClientId();
	 
		  // get confirm password
		  UIInput uiInputConfirmPassword = (UIInput) components.findComponent("confirmPassword");
		  String confirmPassword = uiInputConfirmPassword.getLocalValue() == null ? ""
			: uiInputConfirmPassword.getLocalValue().toString();
	 
		  // Let required="true" do its job.
		  if (password.isEmpty() || confirmPassword.isEmpty()) {
			return;
		  }
	 
		  if (!password.equals(confirmPassword)) {
	 
			FacesMessage msg = new FacesMessage("La password confermata non corrisponde");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			fc.addMessage(passwordId, msg);
			fc.renderResponse();
	 
		  }
	 
		}
	
}