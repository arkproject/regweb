package it.arkproject.security.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import it.arkproject.model.JsfSpringSecGroupMembers;
import it.arkproject.model.JsfSpringSecGroups;
import it.arkproject.model.JsfSpringSecUsers;

/**
 * Backing bean for JsfSpringSecGroupMembers entities.
 * <p>
 * This class provides CRUD functionality for all JsfSpringSecGroupMembers entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class JsfSpringSecGroupMembersBean implements Serializable
{

   private static final long serialVersionUID = 1L;

   /*
    * Support creating and retrieving JsfSpringSecGroupMembers entities
    */

   private Integer id;

   public Integer getId()
   {
      return this.id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   private JsfSpringSecGroupMembers jsfSpringSecGroupMembers;

   public JsfSpringSecGroupMembers getJsfSpringSecGroupMembers()
   {
      return this.jsfSpringSecGroupMembers;
   }

   public void setJsfSpringSecGroupMembers(JsfSpringSecGroupMembers jsfSpringSecGroupMembers)
   {
      this.jsfSpringSecGroupMembers = jsfSpringSecGroupMembers;
   }

   @Inject
   private Conversation conversation;

   @PersistenceContext(unitName = "regWeb", type = PersistenceContextType.EXTENDED)
   private EntityManager entityManager;

   public String create()
   {

      this.conversation.begin();
      this.conversation.setTimeout(1800000L);
      return "create?faces-redirect=true";
   }

   public void retrieve()
   {

      if (FacesContext.getCurrentInstance().isPostback())
      {
         return;
      }

      if (this.conversation.isTransient())
      {
         this.conversation.begin();
         this.conversation.setTimeout(1800000L);
      }

      if (this.id == null)
      {
         this.jsfSpringSecGroupMembers = this.example;
      }
      else
      {
         this.jsfSpringSecGroupMembers = findById(getId());
      }
   }

   public JsfSpringSecGroupMembers findById(Integer id)
   {

      return this.entityManager.find(JsfSpringSecGroupMembers.class, id);
   }

   /*
    * Support updating and deleting JsfSpringSecGroupMembers entities
    */

   public String update()
   {
      this.conversation.end();

      try
      {
         if (this.id == null)
         {
            this.entityManager.persist(this.jsfSpringSecGroupMembers);
            return "search?faces-redirect=true";
         }
         else
         {
            this.entityManager.merge(this.jsfSpringSecGroupMembers);
            return "view?faces-redirect=true&id=" + this.jsfSpringSecGroupMembers.getJsfSpringSecGroupMembersId();
         }
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   public String delete()
   {
      this.conversation.end();

      try
      {
         JsfSpringSecGroupMembers deletableEntity = findById(getId());
         JsfSpringSecGroups jsfSpringSecGroups = deletableEntity.getJsfSpringSecGroups();
         jsfSpringSecGroups.getJsfSpringSecGroupMemberses().remove(deletableEntity);
         deletableEntity.setJsfSpringSecGroups(null);
         this.entityManager.merge(jsfSpringSecGroups);
         JsfSpringSecUsers jsfSpringSecUsers = deletableEntity.getJsfSpringSecUsers();
         jsfSpringSecUsers.getJsfSpringSecGroupMemberses().remove(deletableEntity);
         deletableEntity.setJsfSpringSecUsers(null);
         this.entityManager.merge(jsfSpringSecUsers);
         this.entityManager.remove(deletableEntity);
         this.entityManager.flush();
         return "search?faces-redirect=true";
      }
      catch (Exception e)
      {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
         return null;
      }
   }

   /*
    * Support searching JsfSpringSecGroupMembers entities with pagination
    */

   private int page;
   private long count;
   private List<JsfSpringSecGroupMembers> pageItems;

   private JsfSpringSecGroupMembers example = new JsfSpringSecGroupMembers();

   public int getPage()
   {
      return this.page;
   }

   public void setPage(int page)
   {
      this.page = page;
   }

   public int getPageSize()
   {
      return 10;
   }

   public JsfSpringSecGroupMembers getExample()
   {
      return this.example;
   }

   public void setExample(JsfSpringSecGroupMembers example)
   {
      this.example = example;
   }

   public void search()
   {
      this.page = 0;
   }

   public void paginate()
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

      // Populate this.count

      CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
      Root<JsfSpringSecGroupMembers> root = countCriteria.from(JsfSpringSecGroupMembers.class);
      countCriteria = countCriteria.select(builder.count(root)).where(
            getSearchPredicates(root));
      this.count = this.entityManager.createQuery(countCriteria)
            .getSingleResult();

      // Populate this.pageItems

      CriteriaQuery<JsfSpringSecGroupMembers> criteria = builder.createQuery(JsfSpringSecGroupMembers.class);
      root = criteria.from(JsfSpringSecGroupMembers.class);
      TypedQuery<JsfSpringSecGroupMembers> query = this.entityManager.createQuery(criteria
            .select(root).where(getSearchPredicates(root)));
      query.setFirstResult(this.page * getPageSize()).setMaxResults(
            getPageSize());
      this.pageItems = query.getResultList();
   }

   private Predicate[] getSearchPredicates(Root<JsfSpringSecGroupMembers> root)
   {

      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
      List<Predicate> predicatesList = new ArrayList<Predicate>();

      int jsfSpringSecGroupMembersId = this.example.getJsfSpringSecGroupMembersId();
      if (jsfSpringSecGroupMembersId != 0)
      {
         predicatesList.add(builder.equal(root.get("jsfSpringSecGroupMembersId"), jsfSpringSecGroupMembersId));
      }
      JsfSpringSecGroups jsfSpringSecGroups = this.example.getJsfSpringSecGroups();
      if (jsfSpringSecGroups != null)
      {
         predicatesList.add(builder.equal(root.get("jsfSpringSecGroups"), jsfSpringSecGroups));
      }
      JsfSpringSecUsers jsfSpringSecUsers = this.example.getJsfSpringSecUsers();
      if (jsfSpringSecUsers != null)
      {
         predicatesList.add(builder.equal(root.get("jsfSpringSecUsers"), jsfSpringSecUsers));
      }

      return predicatesList.toArray(new Predicate[predicatesList.size()]);
   }

   public List<JsfSpringSecGroupMembers> getPageItems()
   {
      return this.pageItems;
   }

   public long getCount()
   {
      return this.count;
   }

   /*
    * Support listing and POSTing back JsfSpringSecGroupMembers entities (e.g. from inside an
    * HtmlSelectOneMenu)
    */

   public List<JsfSpringSecGroupMembers> getAll()
   {

      CriteriaQuery<JsfSpringSecGroupMembers> criteria = this.entityManager
            .getCriteriaBuilder().createQuery(JsfSpringSecGroupMembers.class);
      return this.entityManager.createQuery(
            criteria.select(criteria.from(JsfSpringSecGroupMembers.class))).getResultList();
   }

   @Resource
   private SessionContext sessionContext;

   public Converter getConverter()
   {

      final JsfSpringSecGroupMembersBean ejbProxy = this.sessionContext.getBusinessObject(JsfSpringSecGroupMembersBean.class);

      return new Converter()
      {

         @Override
         public Object getAsObject(FacesContext context,
               UIComponent component, String value)
         {

            return ejbProxy.findById(Integer.valueOf(value));
         }

         @Override
         public String getAsString(FacesContext context,
               UIComponent component, Object value)
         {

            if (value == null)
            {
               return "";
            }

            return String.valueOf(((JsfSpringSecGroupMembers) value).getJsfSpringSecGroupMembersId());
         }
      };
   }

   /*
    * Support adding children to bidirectional, one-to-many tables
    */

   private JsfSpringSecGroupMembers add = new JsfSpringSecGroupMembers();

   public JsfSpringSecGroupMembers getAdd()
   {
      return this.add;
   }

   public JsfSpringSecGroupMembers getAdded()
   {
      JsfSpringSecGroupMembers added = this.add;
      this.add = new JsfSpringSecGroupMembers();
      return added;
   }
}