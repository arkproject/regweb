/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.arkproject.util;

import java.util.HashMap;
import java.util.Map;

public final class CodiceFiscaleGeneratore {

    private static boolean isConsonante(char carattere) {
        String vocali = "AEIOU";
        int indice = vocali.indexOf(carattere);
        if (indice < 0) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean isNumero(char carattere) {

        int numVal = Character.getNumericValue(carattere);
        if (numVal < 10 & numVal > -1) {
            return true;
        } else {
            return false;
        }
    }

    private static String calcolaCognome(String cognome) {
        String stringaCognome = "";
        cognome = cognome.replace(" ","");
        cognome = cognome.toUpperCase();

        //creazione stringa con solo consonanti
        if (cognome.length() > 2) {
            
            
            // prendo tutte le consonanti
            for (int i = 0; i < cognome.length(); i++) {
                stringaCognome += CodiceFiscaleGeneratore.isConsonante(cognome.charAt(i)) ? cognome.charAt(i) : "";
            }
            //cognome lungo (consonanti >3)
            if (stringaCognome.length() > 3) {
                stringaCognome = stringaCognome.substring(0, 3);
            }
            
            //cognome corto (consonanti <3)
            if (stringaCognome.length() < 3) {
//                for (int i = 0; i < 3 - stringaCognome.length(); i++) {
//                    stringaCognome += CodiceFiscaleGeneratore.isConsonante(cognome.charAt(i)) ? "" : cognome.charAt(i);
//                }
                
                // aggiungo le vocali al cognome in quanto non bastano le consonanti
                for (int i = 0; i < cognome.length(); i++) {
                    if (!CodiceFiscaleGeneratore.isConsonante(cognome.charAt(i))){
                        stringaCognome +=cognome.charAt(i);
                    }                  
                }
                stringaCognome = stringaCognome.substring(0, 3);
            }
            
            
        } //cognome cortissimo (2 lettere)
        else {
            stringaCognome = cognome;
            for (int i = 0; i < 3 - cognome.length(); i++) {
                stringaCognome += "X";
            }
        }
        return stringaCognome;
    }

    private static String calcolaNome(String nome) {
        String stringaNome = "";
        nome = nome.replace(" ","");
        nome = nome.toUpperCase();

        //creazione stringa con solo consonanti
        if (nome.length() > 2) {
            for (int i = 0; i < nome.length(); i++) {
                stringaNome += CodiceFiscaleGeneratore.isConsonante(nome.charAt(i)) ? nome.charAt(i) : "";
            }
            //nome lungo (consonanti >3)
            if (stringaNome.length() > 3) {
                stringaNome = stringaNome.substring(0, 1) + stringaNome.substring(2, 4);
            }
            //nome corto (consonanti <3)
            if (stringaNome.length() < 3) {
                int i = 0;
                while (stringaNome.length() < 3) {
                    stringaNome += CodiceFiscaleGeneratore.isConsonante(nome.charAt(i)) ? "" : nome.charAt(i);
                    i++;
                }
            }
        } //nome cortissimo (2 lettere)
        else {
            stringaNome = nome;
            for (int i = 0; i < 3 - nome.length(); i++) {
                stringaNome += "X";
            }
        }
        return stringaNome;
    }

    private static String calcolaAnno(int anno) {
        anno = anno % 100;
        return Integer.toString(anno);
    }

    private static char calcolaMese(int mese) {
        String elencomesi = "ABCDEHLMPRST";
        return elencomesi.charAt(mese - 1);
    }

    private static String calcolaGiornoSesso(int giorno, String sesso) {
        if (sesso.equals("M")) {
            if (giorno < 10) {
                return "0" + Integer.toString(giorno);
            } else {
                return Integer.toString(giorno);
            }
        } else {
            return Integer.toString(giorno + 40);
        }
    }

    private static String calcolaComune(String comune) {
        comune = comune.toUpperCase();
        Map<String, String> codici = new HashMap<>();
        codici.put("BOLOGNA", "A944");
        codici.put("MILANO", "F205");
        return codici.get(comune);
    }

    private static char calcolaCarControllo(String codice) {
                    //Sostituisce ai numeri dentro codice lettere da A a J e calcola il valore di ogni carattere
        //distinguendo fra posizioni pari e dispari

        String caratteri = "ABCDEFGHIJ";
        String pesiPari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String pesiDispari = "BAKPLCQDREVOSFTGUHMINJWZYX";

        int somma = 0;

        for (int i = 0; i < codice.length(); i++) {

            if (CodiceFiscaleGeneratore.isNumero(codice.charAt(i))) {
                codice = codice.replace(codice.charAt(i), caratteri.charAt(Character.getNumericValue(codice.charAt(i))));
            }
            if ((i + 1) % 2 == 0) {
                somma += pesiPari.indexOf(codice.charAt(i));
            } else {
                somma += pesiDispari.indexOf(codice.charAt(i));
            }
        }

        somma = somma % 26;
        return pesiPari.charAt(somma);
    }

    public static String calcolaCodiceFiscaleGeneratore(String nome, String cognome, int giorno, int mese, int anno, String sesso, String comune) {
        String codfis = calcolaCognome(cognome) + calcolaNome(nome) + calcolaAnno(anno) + calcolaMese(mese) + calcolaGiornoSesso(giorno, sesso) + comune;
        codfis += calcolaCarControllo(codfis);
        return codfis;
    }

    private static boolean isOmocodico(String codiceBase, String codiceConfronto) {
        String strOmocodia = "LMNPQRSTUV";
        boolean test = true;

        for (int i = 0; i < codiceBase.length() - 1; i++) {
            if (codiceBase.charAt(i) != codiceConfronto.charAt(i)
                    && strOmocodia.indexOf(codiceConfronto.charAt(i)) != Character.getNumericValue(codiceBase.charAt(i))) {
                test = false;
            }
        }
        return test;
    }

    public static boolean verificaCodiceFiscaleGeneratore(String nome, String cognome, int giorno, int mese, int anno, String sesso, String comune, String codiceFiscale) {
        String codfisCalc = calcolaCognome(cognome) + calcolaNome(nome) + calcolaAnno(anno) + calcolaMese(mese) + calcolaGiornoSesso(giorno, sesso) + calcolaComune(comune);
        codfisCalc += calcolaCarControllo(codfisCalc);

        System.out.println(codiceFiscale.length() + " " + codiceFiscale.substring(0, 14));

        if ((codiceFiscale.length() != 16) | (calcolaCarControllo(codiceFiscale.substring(0, 15)) != codiceFiscale.charAt(15))) {
            System.out.println("Codice inserito non valido");
            return false;
        } else if (codfisCalc.equals(codiceFiscale)) {
            return true;
        } else if (CodiceFiscaleGeneratore.isOmocodico(codfisCalc, codiceFiscale)) {
            System.out.println("Codice omocodico");
            return true;
        } else {
            return false;
        }
    }

}
