package it.arkproject.util;

import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

public class JsfUtil {
	
    // Trasforma numero stringa in integer
    public static Integer getKey(String value) {
        java.lang.Integer key;
        key = Integer.valueOf(value);
        return key;
    }

    public static String getParam(FacesContext fc, String param) {
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        return params.get(param);
    }

    public static SelectItem[] getSelectItems(List<?> entities, boolean selectOne) {
        int size = selectOne ? entities.size() + 1 : entities.size();
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        if (selectOne) {
            items[0] = new SelectItem("", "---");
            i++;
        }
        for (Object x : entities) {
            items[i++] = new SelectItem(x, x.toString());
        }
        return items;
    }

    public static void addErrorMessage(Exception ex, String defaultMsg) {
        String msg = ex.getLocalizedMessage();
        if (msg != null && msg.length() > 0) {
            addErrorMessage(msg);
        } else {
            addErrorMessage(defaultMsg);
        }
    }

    public static void addErrorMessages(List<String> messages) {
        for (String message : messages) {
            addErrorMessage(message);
        }
    }

    public static void addErrorMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addErrorMessage(String msg, String msgDettagli) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msgDettagli);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addSuccessMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addSuccessMessageDettagli(String msg, String msgDettagli) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msgDettagli);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static String getRequestParameter(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }

    public static Object getObjectFromRequestParameter(String requestParameterName, Converter converter, UIComponent component) {
        String theId = JsfUtil.getRequestParameter(requestParameterName);
        return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
    }

    public static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }

    public static enum PersistAction {
        CREATE,
        DELETE,
        UPDATE
    }

    public static enum EntitaNomeScheda {
        Anagraficapaziente("Scheda Anagrafica"),
        Comune("Scheda Indirizzo di residenza"),
        Demografica("Scheda Demografica"),
        Eventopaziente("Evento Paziente"),
        Indirizzoresidenza("Indirizzo di Residenza"),
        Mmse("Mini Mental State Evaluation"),
        Cirs("C.I.R.S"),
        Provinciaregione("Provincia Regione"),
        Sesso("Sesso"),
        User("User"),
        TableTest("Tabletest");

        private final String text;

        private EntitaNomeScheda(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }
 
}

//    public static enum NomeScheda{
//        "Scheda Anagrafica",
//        "Scheda Indirizzo di residenza",
//        "Scheda Demografica",
//        "Anamnesi Fisiologica",
//        "Anamnesi Patologica",
//        "Familiarità",
//        "Scheda farmacologica",
//        "Esame cognitivo",
//        "Esami di laboratorio",
//        "Esami Strumentali",
//        "Test CIRS",
//        "Test ADL",
//        "Test IADL",
//        "Test MMSE",
//        "Test GDS",
//        "Test ACER R",
//        "Test MOCA",
//        "Test MODA",
//        "Test NPI",
//        "Caregiver Anagrafica",
//        "Caregiver Test Stress",
//        "Scheda di Diagnosi"
//    }

