package it.arkproject.genericClass;

import java.util.List;

public interface IAbstractBean<T>  {
	public String create();
	public String update();
	public String delete();
	public void retrieve();
	public T findById (Integer id);
	public List<T> getAll();

}
