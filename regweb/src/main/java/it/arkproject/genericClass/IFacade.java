package it.arkproject.genericClass;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

@Local
public interface IFacade<T>  {
	public void create (T entity);
	public void edit (T entity);
	public void delete (T entity);
	public T find(Class type,Object id);
	public List<T> getAll();
	public void detachEntity(T entity);
	
}
