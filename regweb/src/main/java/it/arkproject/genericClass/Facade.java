package it.arkproject.genericClass;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Stateful
public class Facade<T> implements IFacade<T> {


	private static final String UNIT_NAME = "regWeb";

	@PersistenceContext(unitName = UNIT_NAME, type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;



	@Override
	public void create(T entity) {
		entityManager.persist(entity);
	}

	@Override
	public void edit(T entity) {
		entityManager.merge(entity);
	}

	@Override
	public void delete(T entity) {
		entityManager.remove(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T find(Class type, Object id) {
		return (T) this.entityManager.find(type, id);
	}

	@Override
	public List<T> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void detachEntity(T entity) {
		this.entityManager.detach(entity);
	}

}
