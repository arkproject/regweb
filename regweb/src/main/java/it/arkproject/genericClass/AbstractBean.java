package it.arkproject.genericClass;

import it.arkproject.model.Anagraficapaziente;
import it.arkproject.util.JsfUtil;
import it.arkproject.view.AnagraficaPazienteBean;
import it.arkproject.view.IndirizzoresidenzaBean;
import it.arkproject.view.Util.NavigationManager;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Dependent
public abstract class AbstractBean<T extends Serializable> implements
		IAbstractBean<T>, Serializable {

	private static final long serialVersionUID = 1L;

	private NavigationManager navigationManager = new NavigationManager();

	@Inject
	AnagraficaPazienteBean anagraficaPazienteBean;

	@EJB
	private IFacade<T> facade;

	private Class<T> entityClass;

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		Type type = genericSuperclass.getActualTypeArguments()[0];
		if (type instanceof Class) {
			this.entityClass = (Class<T>) type;
		} else if (type instanceof ParameterizedType) {
			this.entityClass = (Class<T>) ((ParameterizedType) type)
					.getRawType();
		}
		navigationManager.setNomeScheda(entityClass.getSimpleName());
	}

	private Integer id;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private Integer idTest = 100;

	public Integer getIdTest() {
		return this.idTest;
	}

	public void setIdTest(Integer idTest) {
		this.id = idTest;
	}

	private T current;

	public T getCurrent() {
		return current;
	}

	public void setCurrent(T current) {
		this.current = current;
	}

	@Inject
	private Conversation conversation;

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	private Integer pazienteId;

	public Integer getPazienteId() {
		return pazienteId;
	}

	public void setPazienteId(Integer pazienteId) {
		this.pazienteId = pazienteId;
	}

	private Date dataCompilazione;

	public Date getDataCompilazione() {
		return dataCompilazione;
	}

	public void setDataCompilazione(Date dataCompilazione) {
		this.dataCompilazione = dataCompilazione;
	}

	@Override
	public void retrieve() {
		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}
		this.conversation.end();
		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}
		if (id != null) {
			this.current = findById(id);
			// facade.detachEntity(this.current);
			 System.out.println(id.toString());
		}
	}

	@Override
	public String create() {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		if (facesContext.getExternalContext().getRequestParameterMap()
				.get("pazienteId") != null) {
			pazienteId = Integer.parseInt(facesContext.getExternalContext()
					.getRequestParameterMap().get("pazienteId"));
		}

		this.id = null;

		try {
			this.current = entityClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return navigationManager.gethRef();
	}

	@Override
	public String update() {
		try {
			if (this.id == null) {
				Field[] fields = this.current.getClass().getDeclaredFields();
				for (Field field : fields) {
					field.setAccessible(true);
					// se inserisco una nuova scheda devo passare l'id del
					// paziente alla scheda
					if (field.getName() == "anagraficapaziente") {
						field.set(current, new Anagraficapaziente(pazienteId));
						break;
					}
					// System.out.println("Field Name: " + field.getName());
					// System.out.println("Field Type: " + field.getType());
					// System.out.println("Field Value: " + field.get(current));
				}

				facade.create(current);
				JsfUtil.addSuccessMessageDettagli(
						navigationManager.getNomeScheda(), "La scheda "
								+ navigationManager.getNomeScheda()
								+ " e' stata correttamente creata.");
			} else {
				facade.edit(current);
				JsfUtil.addSuccessMessageDettagli(
						navigationManager.getNomeScheda(), "La scheda "
								+ navigationManager.getNomeScheda()
								+ " e' stata correttamente aggiornata.");
			}
			return "/pages/secure/paziente/patientDashboard?faces-redirect=true";
		} catch (Exception e) {
			JsfUtil.addErrorMessage(navigationManager.getNomeScheda(),
					e.getMessage());
			// FacesContext.getCurrentInstance().addMessage(null,
			// new FacesMessage(e.getMessage()));
			// System.out.println(e.getMessage());
			return null;
		}
	}

	// // Controllo sempre se la data di compilazione è maggiore della data di
	// // compilazione scheda anagrafica
	// private boolean checkDataCompilazione() throws IllegalArgumentException,
	// IllegalAccessException {
	// boolean checkDataCompilazione = false;
	//
	// // Field[] fields = this.current.getClass().getDeclaredFields();
	// // for (Field field : fields) {
	// // field.setAccessible(true);
	// // // controllo sempre se la data di compilazione della scheda è
	// // // maggiore della data di compilazione scheda anagrafica
	// // // nel caso sollevo un'eccezione e blocco
	// // if (field.getName() == "dataCompilazione") {
	// // Calendar dataCompilazioneScheda = Calendar.getInstance();
	// // Calendar dataCompilazioneAnagrafica = Calendar.getInstance();
	// // // dataCompilazioneAnagrafica.set(2015, 01, 14);
	// // // dataCompilazioneScheda.set(2015, 01, 15);
	// // dataCompilazioneScheda.setTime((Date) field.get(this.current));
	// // // System.out.println("Field Value: " + field.get(current));
	// // dataCompilazioneAnagrafica.setTime(anagraficaPazienteBean
	// // .getAnagraficapaziente().getDataCompilazione());
	// //
	// // if (!dataCompilazioneScheda.after(dataCompilazioneAnagrafica)
	// // && !dataCompilazioneScheda
	// // .equals(dataCompilazioneAnagrafica)) {
	// // checkDataCompilazione = true;
	// // return checkDataCompilazione;
	// // }
	// //
	// // }
	// // System.out.println("Field Name: " + field.getName());
	// // System.out.println("Field Type: " + field.getType());
	// // System.out.println("Field Value: " + field.get(current));
	// }
	// return checkDataCompilazione;
	// }

	@Override
	public String delete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T findById(Integer id) {
		return facade.find(entityClass, id);
	}

	public void validateDate(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {

			String msg = "Data di compilazione minore della data di compilazione anagrafica.";

			Calendar dataCompilazioneScheda = Calendar.getInstance();
			Calendar dataCompilazioneAnagrafica = Calendar.getInstance();
			// // dataCompilazioneAnagrafica.set(2015, 01, 14);
			// // dataCompilazioneScheda.set(2015, 01, 15);
			dataCompilazioneScheda.setTime((Date) value);
			dataCompilazioneAnagrafica.setTime(anagraficaPazienteBean
					.getAnagraficapaziente().getDataCompilazione());

			if (!dataCompilazioneScheda.after(dataCompilazioneAnagrafica)
					&& !dataCompilazioneScheda
							.equals(dataCompilazioneAnagrafica)) {
				FacesMessage message = new FacesMessage();
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				message.setSummary(msg);
				throw new ValidatorException(message);
			}
		}
	}

}
